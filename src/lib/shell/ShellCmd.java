package lib.shell;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * A class that is repsonsible for executing a system executable
 * file (batch script, etc).
 * 
 * @author Paul Austen
 */
public class ShellCmd implements Runnable
{
	private int processExitValue;
	private StringBuffer stdOut;
	private StringBuffer stdErr;
	private int outputStreamPollPeriod;
	private Process p;
	private boolean debug;
    private String lastStdOut;
    private String lastStdErr;
    
	/**
	 * Constructor
	 */
	public ShellCmd(boolean debug)
	{
        this.debug=debug;
		outputStreamPollPeriod=100;
        Thread t = new Thread(this);
        Runtime.getRuntime().addShutdownHook(t);
	}
    public ShellCmd()
    {
        this(false);
    }
    
    /**
     * Enable/disabled debug (print cmd and env to stdout)
     * @param debug
     */
    public void setDebug(boolean debug) { this.debug=debug; }
	
    /**
     * Thread executed on shutdown which closes any active child process
     * when the application shuts down
     */
    public void run()
    {
      if( p != null )
      {
        p.destroy();
      }
    }
    
	/**
	 * Run a system command.
	 * 
	 * @param args An aray of arguments. args[0] must be the file to be excuted.
	 * @param env An array containing a list of all enviromental variables in the
	 * format VAR=VALUE.
	 * @return The exit code of the process
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public synchronized int runSysCmd(String args[],String env[]) throws IOException,InterruptedException
	{
        //debug
        if( debug)
        {
          System.out.println("ENVIRONMENT:");
          for( int i=0 ; i<env.length ; i++ )
          {
            System.out.print(env[i]+" ");
          }
          System.out.println();
          System.out.println("COMMAND LINE:");
          for( int i=0 ; i<args.length ; i++ )
          {
            System.out.print(args[i]+" ");
          }
          System.out.println();
        }
        
		processExitValue=-1;
		stdOut = new StringBuffer();
		stdErr = new StringBuffer();
		//p = Runtime.getRuntime().exec(args,env);
        ProcessBuilder pb = new ProcessBuilder(args);
              
        Map<String, String> _env = pb.environment();
        //Clear the env and add the one supplied
        _env.clear();
        StringTokenizer strTok;
        String key,value;for( int i=0 ; i<env.length ; i++)
        {
          strTok = new StringTokenizer(env[i],"=");
          if( strTok.countTokens() == 2 )
          {
            key=strTok.nextToken();
            value=strTok.nextToken();
            _env.put(key, value);
          }
        }

        p = pb.start();
		while(true)
		{
		  try
		  {
			processExitValue=p.exitValue();
			break;
		  }
		  //the process is still running if we got here
		  catch(IllegalThreadStateException e)
		  {
			//Update any output from stdErr. 
			//On a windows system stderr will not receive any data even 
			//if an error occurs
			while( p.getErrorStream().available() > 0 )
			{
				stdErr.append((char)p.getErrorStream().read());
			}
			//Update any output from stdOut
			while( p.getInputStream().available() > 0 )
			{
				stdOut.append((char)p.getInputStream().read());
			}
          }
          Thread.sleep(outputStreamPollPeriod);
		}
		//Return the process return code
		return processExitValue;
	}

	/**
	 * Run a system command. Uses the same enviromental variables as the program 
	 * that calls this method.
	 * 
	 * @param args args An aray of arguments. args[0] must be the file to be excuted.
	 * @return The process exit code
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public int runSysCmd(String args[]) throws IOException,InterruptedException
	{
		return runSysCmd(args,getEnvVars());
	}
	
	
	/**
	 * Run a system command. Uses the same enviromental variables as the program 
	 * that calls this method.
	 * @param argString A space or tab seprated list of all program arguments. The first being the program name.
	 * @return The process exit code
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public int runSysCmd(String argString) throws IOException,InterruptedException
	{
		StringTokenizer strTok = new StringTokenizer(argString," \t");
		int argCount=strTok.countTokens();
		String args[] = new String[argCount];
		for( int i=0 ; i<argCount ; i++ )
		{
			args[i]=strTok.nextToken();
		}
		return runSysCmd(args,getEnvVars());
	}
	
	/**
	 * Send some data to a running process. This must be called after runSysCmd has 
	 * been called and before it has returned.
	 * 
	 * @param data The data to be sent to the process.
	 */
	public void sendToProcess(byte data[]) throws IOException
	{
		p.getOutputStream().write(data);
		p.getOutputStream().flush();		
	}
	
	/**
	 * Get a list of all the system enviromental variables. 
	 * @return A String array containing a list of all enviromental vaialbles 
	 * in the format VAR=VALUE.
	 */
	private String[] getEnvVars()
	{
		Set s = System.getenv().entrySet();
		String env[] = new String[s.size()];
		Iterator<Map.Entry> i = s.iterator();
		Map.Entry me;
		int index=0;
		//Build list of all env variables
		while( i.hasNext() )
		{
			me=(Map.Entry)i.next();
			env[index]="" + me.getKey() + "=" + me.getValue();
			index++;
		}
		//Return the list
		return env;
	}
	
	/**
	 * Get the output of the program sent to stdout.
	 * @param onlyReturnNewText If true then only new text sent to std out is returned.
	 * @return The data sent to stdout by a command.
	 */
    public String getStdOut(boolean onlyReturnNewText)
    {
      String newStdOut = stdOut.toString();
      if( onlyReturnNewText && lastStdOut != null )
      {
        if( newStdOut.length() > lastStdOut.length() )
        {
          String textAdded = newStdOut.substring(lastStdOut.length());
          lastStdOut=newStdOut;
          return textAdded;
        }
        return "";
      }
      lastStdOut=newStdOut;
      return newStdOut;
    }
    public String getStdOut()
    {
      return getStdOut(false);
    }
    
	/**
	 * Get the output of the program sent to stderr.
	 * onlyReturnNewText If true then only new text sent to std out is returned.
	 * @return The data sent to stderr by a command.
	 */
    public String getStdErr(boolean onlyReturnNewText)
    {
      String newStdErr = stdErr.toString();
      if( onlyReturnNewText && lastStdErr != null )
      {
        if( newStdErr.length() > lastStdErr.length() )
        {
          String textAdded = newStdErr.substring(lastStdErr.length());
          lastStdErr=newStdErr;
          return textAdded;
        }
        return "";
      }
      lastStdErr=newStdErr;
      return newStdErr;
    }
    public String getStdErr()
    {
      return getStdErr(false);
    }
    
	/**
	 * Get the last process exit value.
	 * 
	 * @return The process exit code from the last time that 
	 * runSysCmd was executed. Only valid after runSysCmd has returned.
	 */
	public int getProcessExitValue()
	{
		return processExitValue;
	}
	
	
	/**
	 * Entry point for test of the ShellCmd class on a windows 2000 host machine
	 * by running the command interpreter, then a dir command and finally exiting 
	 * the command interpreter. Note that no data is returned on stdout until the 
	 * system command has comlpleted.
	 */
	public static void main(String args[])
	{
		try
		{
			ShellCmd sc = new ShellCmd();
			DoCmd dc = new DoCmd(sc);
			dc.start();
			sc.runSysCmd("c:\\windows\\system32\\cmd.exe" );
			
			System.out.println("STDOUT    : " + sc.getStdOut());
			System.out.println("STDERR    : " + sc.getStdErr());
			System.out.println("EXIT CODE : " + sc.getProcessExitValue());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
}

/**
 * A class used to test ShellCmd by sending commands to a 
 * running process.
 * 
 * @author Paul Austen
 *
 */
class DoCmd extends Thread
{
	private ShellCmd shellCmd;
	public DoCmd(ShellCmd shellCmd)
	{
		this.shellCmd=shellCmd;
	}

	public void run()
	{
		try
		{
			System.out.println("WAITING...");
			Thread.sleep(1000);
			System.out.println("SENDING DIR");
			shellCmd.sendToProcess("DIR\r\n".getBytes());
			System.out.println("WAITING...");
			Thread.sleep(1000);
			System.out.println("SENDING EXIT");
			shellCmd.sendToProcess("EXIT\r\n".getBytes());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
}
