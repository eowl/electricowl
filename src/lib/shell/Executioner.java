package lib.shell;

import lib.gui.Dialogs;
import eowl.view.*;
import eowl.model.*;
import java.util.*;

public class Executioner extends Thread {
	private ElectricOWLFrame 	electricOWLFrame;
	private String 				cmdString;
	private String 				cmdArray[];
	
	public Executioner(ElectricOWLFrame electricOWLFrame, String sensorName, SensorData sensorData, ControllerConfig controllerConfig) {
		this.electricOWLFrame=electricOWLFrame;
		//If no command has been defined then don't go any further
		if( controllerConfig.maxPowerExtCmd == null || controllerConfig.maxPowerExtCmd.length() == 0 ) {
			return;
		}
		
		Vector<String> 		args = new Vector<String>();
		StringTokenizer strTok = new StringTokenizer(controllerConfig.maxPowerExtCmd, "$");
		boolean firstArg=true;
		while( strTok.hasMoreElements() ) {
			String t = strTok.nextToken().trim();
			if( !firstArg ) {
				/*
				 * Arguments that may be provided on the command line
				 * 
				 * $1 = The date the reading was taken in the format yyyy-MM-dd HH:mm:ss
				 * $2 = The sensor address
				 * $3 = The current Amps
				 * $4 = The current KW
				 * $5 = the cost
				 * $6 = The log filename (absolute path)
				 * %7 = The max power trigger value (KW)
				 * $8 = The configured currency
				 * $
				 */
				//Sensor name should be included in the arguments
				if( t.equals("1") ) {
					t=sensorData.getDateString();
				}
				else if( t.equals("2") ) {
					t=""+sensorData.getAddress();
				}
				else if( t.equals("3") ) {
					t=""+sensorData.getAmps();
				}
				else if( t.equals("4") ) {
					t=""+sensorData.getKW(controllerConfig.voltage);
				}
				else if( t.equals("5") ) {
					t=""+sensorData.getCost(controllerConfig.voltage, controllerConfig.costPerkWh);
				}
				else if( t.equals("6") ) {
					t=""+controllerConfig.logFile.getAbsolutePath();
				}
				else if( t.equals("7") ) {
					t=""+controllerConfig.maxPowerTriggerKWh;
				}
				else if( t.equals("8") ) {
					t=""+controllerConfig.currencyString;
				}
				else {
					electricOWLFrame.printStatus("External command is invalid: "+t+" is not a valid argument in ("+controllerConfig.maxPowerExtCmd+"). $1-$8 are valid.");
				}
			}
			//the first arg should be the executable name
			firstArg=false;
			args.add(t);
		}
		if( args.size() == 0  ) {
			electricOWLFrame.printStatus("Not external command found to execute.");
		}
		cmdArray = new String[args.size()];
		int index=0;
		StringBuffer cmdStringBuffer = new StringBuffer();
		for( String arg : args ) {
			cmdArray[index]=arg;
			cmdStringBuffer.append(arg+" ");
			index++;
		}
		this.cmdString=cmdStringBuffer.toString().trim();
	}
		
	public void run() {
		//If we have no command to execute then don't try
		//This allows users to configure a maximum power that will be 
		//displayed on the graph but not acutally fire an external command
		if( cmdArray == null ) {
			return;
		}
		ShellCmd sc = new ShellCmd();
		try {
			electricOWLFrame.printStatus("Started External command: "+cmdString);
			sc.runSysCmd(cmdArray);
			electricOWLFrame.printStatus("External command completed: "+cmdString);
			electricOWLFrame.printStatus(sc.getStdOut());
		}
		catch(Exception ex) {
			String message = "Failed to execute external shell command";
			if( ex.getLocalizedMessage() != null && ex.getLocalizedMessage().length() > 0 ) {
				message = ex.getLocalizedMessage();
			}
			Dialogs.showErrorDialog(electricOWLFrame, "Error", message);
		}
	}
}	

