/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.crypt;

import sun.misc.*;

import java.io.*;

/**
 * Responsible for providing simple encryption decryption functionality for Strings.
 */
public class Crypt
{
  private DesCipher     des;
  
  /**
   * Constructor
   * 
   * @param keyString This key must be 8 characters long.
   */
  public Crypt(String keyString) throws IOException
  {
    if( keyString.length() != 8 )
    {
      throw new IOException("SimpleConfig encryption key must be 8 characters long");
    }
    des = new DesCipher(keyString.getBytes());
  }
  
  /**
   * Get the encrypted text.
   * 
   * @param text The text to encrypt
   * @return The des/base64 encrypted text or null if text is null or text length is 0
   */
  public String getEncrypted(String text)
  {
    if( text == null || text.length() == 0 )
    {
      return null;
    }
    StringBuffer textBuffer = new StringBuffer(text);
    int remainder=(text.length()+1)%8;
    if( remainder > 0 )
    {
      int bytesToAdd=8-remainder+1;
      for( int i=0 ; i<bytesToAdd ; i++ )
      {
        textBuffer.append((char)bytesToAdd);
      }
    }
    else
    {
      //Set the last char to 1 (one pad char)
      textBuffer.append((char)1);
    }
    byte textBytes[]=textBuffer.toString().getBytes();
    int offset=0;
    while(true)
    {
      des.encrypt(textBytes, offset, textBytes, offset);
      offset+=8;
      if( offset > text.length() )
      {
        break;
      }
    }
    BASE64Encoder encoder = new BASE64Encoder();
    String base64 = encoder.encode(textBytes);
    return base64;
  }
  
  /**
   * Get the decrypted text given the encrypted text.
   * 
   * @param base64 The encrypted text
   * @return The decrypted text or null if base64 is null or base64 length is 0
   * @throws IOException
   */
  public String getDecrypted(String base64) throws IOException
  {
    if( base64 == null || base64.length() == 0 )
    {
      return null;
    }

    BASE64Decoder decoder = new BASE64Decoder();
    byte textBytes[] = decoder.decodeBuffer(base64);
        
    //As DesCipher decrypts 8 bytes at a time we start from the end of the 
    //String (which must be at least 8 characters long) and encrypt.
    int offset=0;
    while(true)
    {
      des.decrypt(textBytes, offset, textBytes, offset);
      offset+=8;
      if( offset >= textBytes.length )
      {
        break;
      }
    }
    //The last character holds the number of padding characters to be removed
    int padCount=textBytes[textBytes.length-1];
    String text = new String(textBytes); 
    try
    {
      return text.substring(0,text.length()-padCount);
    }
    catch(java.lang.StringIndexOutOfBoundsException e) 
    {
      return null;
    }
  }
 
}
