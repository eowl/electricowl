/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.gui;

import  java.io.*;

/**
 * Filter class used with JFileChoosers in order to filter on a given extension.
 */
public class FileExtensionFilter extends javax.swing.filechooser.FileFilter
{
  String  extension="";
  String  description="";

  /**
   * Constructor
   */
  public FileExtensionFilter()
  {
  }

  /**
   * Constructor
   * pass the
   */
  public FileExtensionFilter(String extension,String description)
  {
    this.extension=extension;
    this.description=description;
  }

  /**
    This is the one of the methods that is declared in
    the abstract class
   */
  public boolean accept(File f)
  {
    //if it is a directory -- we want to show it so return true.
    if (f.isDirectory())
      return true;

    //get the extension of the file
    String extension = getExtension(f);

    //check to see if the extension is equal to script
    if ((extension.toLowerCase().equals(extension.toLowerCase()) ))
      return true;

    //default -- fall through. False is return on all
    //occasions except:
    //a) the file is a directory
    //b) the file's extension is what we are looking for.
    return false;
  }

  /**
    Again, this is declared in the abstract class

    The description of this filter
   */
  public String getDescription()
  {
      return description;
  }

  /**
    Method to get the extension of the file, in lowercase
   */
  private String getExtension(File f)
  {
    String s = f.getName();
    int i = s.lastIndexOf('.');
    if (i > 0 && i < s.length() - 1)
      return s.substring(i+1).toLowerCase();
    return "";
  }

  public void setExtension(String extension,String description)
  {
    this.extension=extension;
    this.description=description;
  }
}