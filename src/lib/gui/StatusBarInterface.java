/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.gui;

public interface StatusBarInterface
{
  public abstract void println(String line);
  public abstract void println_persistent(String line);
}
