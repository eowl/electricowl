/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.gui;

import java.awt.*; 

/**
 * General user interface class. A container for static methods useful in
 * building user interfaces.
 * @author Paul Austen
 */
public class UI 
{
  /**
   * The vertical space in pixels between all displayed components.
   */
  public static final int VSPACE=5;
  /**
   * The horizontal space in pixels between all displayed components.
   */
  public static final int HSPACE=5;

  
  /**
   * Center the currentComponent over the the parentComponent.
   *
   * @param  parentComponent   The parent component over which the currentComponent is to be centered.
   * @param  currentComponent  The component to be centered.
   */
  public static void CenterInParent(Component parentComponent, Component currentComponent) 
  {
      Point parentComponentLocation;
      Dimension parentComponentSize;
      int xPos;
      int yPos;
      //get the top left hand corner of the parent frame
      parentComponentLocation = parentComponent.getLocation();
      //get the size of the parent frame
      parentComponentSize = parentComponent.getSize();
      //calc xpos
      xPos = ((parentComponentSize.width / 2) - (currentComponent.getSize().width / 2)) + parentComponentLocation.x;
      //if off screen then set it back on
      if (xPos < 0) {
          xPos = 0;
      }
      //calc ypos
      yPos = ((parentComponentSize.height / 2) - (currentComponent.getSize().height / 2)) + parentComponentLocation.y;
      //if off screen then set it back on
      if (yPos < 0) {
          yPos = 0;
      }
      //set location of the currentFrame
      currentComponent.setLocation(xPos, yPos);
  }

	/**
	 * Center the currentFrame on the computer screen.
	 *
	 * @param  currentComponent  The component to be centered on the screen.
	 */
	public static void CenterOnScreen(Component currentComponent) 
	{
		Dimension screenSize;
		//the current screen size
		int DlgWidth;
		//the current screen size
		int DlgHeigth;
		//the dialog width and height
		screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		DlgHeigth = currentComponent.getSize().height;
		DlgWidth = currentComponent.getSize().width;
		//if dialog size larger than screen size then set dialog size = screen size
		if (DlgHeigth > screenSize.height) {
			DlgHeigth = screenSize.height;
		}
		if (DlgWidth > screenSize.width) {
			DlgWidth = screenSize.width;
		}
		currentComponent.setLocation((screenSize.width - DlgWidth) / 2, (screenSize.height - DlgHeigth) / 2);
	}

	  /**
	   *  Attempt to find the a Frame object in the component hierarchy.
	   *    @param  orgComponent The component to start the search from.
	   *    @return The Frame object found or null if none was found.
	   */
	  public static Frame GetParentFrame(Component orgComponent)
	  {
	    Component component;
	    component = orgComponent;
	    while (true)
	    {
	      component = component.getParent();
	      if (component == null)
	      {
	                return null;
	      }
	      else
	      {
	        if (component instanceof Frame)
	        {
	          return (Frame)component;
	        }
	      }
	    }
	  }
	  

}

