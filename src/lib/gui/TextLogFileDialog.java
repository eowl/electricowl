/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.gui;


import  java.io.*;
import  java.awt.*;
import  javax.swing.*;

public class TextLogFileDialog
{
  private JFileChooser        selectFileDialog;
  private FileExtensionFilter textFileFilter,docFileFilter;
  private DecimalNumberField  fileSizeJTextField;
  private JCheckBox           appendJcheckBox;
  private JCheckBox           rollAroundJcheckBox;
  private JCheckBox           addTimeStampJcheckBox;
  private static int          maxLogFileSize=1048536;
  private RowPane             rowPane;

  public TextLogFileDialog()
  {
    selectFileDialog = new JFileChooser();
    textFileFilter = new FileExtensionFilter("txt","Text File (*.txt)");
    selectFileDialog.addChoosableFileFilter(textFileFilter);
    docFileFilter = new FileExtensionFilter();
    docFileFilter.setExtension("doc","Document File (*.doc)");
    selectFileDialog.addChoosableFileFilter(docFileFilter);
    selectFileDialog.setFileFilter(docFileFilter);
    appendJcheckBox = new JCheckBox("Append");
    rollAroundJcheckBox = new JCheckBox("Roll Around");
    addTimeStampJcheckBox = new JCheckBox("Add Time Stamps");

    rowPane = new RowPane();

    rowPane.add( addTimeStampJcheckBox );
    rowPane.add( rollAroundJcheckBox );
    rowPane.add( appendJcheckBox );
    
    rowPane.add( addTimeStampJcheckBox );
    rowPane.add( rollAroundJcheckBox );
    rowPane.add( appendJcheckBox );
    
    fileSizeJTextField = new DecimalNumberField();
    fileSizeJTextField.setText( "" + maxLogFileSize );
    fileSizeJTextField.setColumns(6);
    rowPane.add( fileSizeJTextField );
    rowPane.add( new JLabel("File size (bytes)") );
    rowPane.add( new JLabel("OPTIONS") );
    rowPane.add( fileSizeJTextField );
    rowPane.add( new JLabel("File size (bytes)") );
    rowPane.add( new JLabel("OPTIONS") );
    selectFileDialog.setAccessory( rowPane );
    appendJcheckBox.setSelected(true);
    rollAroundJcheckBox.setSelected(true);
    addTimeStampJcheckBox.setSelected(false);
  }
  public void rescanCurrentDirectory()
  {
    selectFileDialog.rescanCurrentDirectory();
  }
  public void removeTimeStamps()
  {
    rowPane.remove(addTimeStampJcheckBox);
  }

  //sets the file size in bytes
  public void setFileSize(long fileSize) { fileSizeJTextField.setText("" + fileSize); }
  public void setTitle(String title) { selectFileDialog.setDialogTitle(title); }
  public void setSelectedFile(File file) { selectFileDialog.setSelectedFile(file); }
  public void setAppendToFile(boolean append) { appendJcheckBox.setSelected(append); }
  public void setRollAround(boolean rollAround) { rollAroundJcheckBox.setSelected(rollAround); }
  public void setAddTimeStamps(boolean addTimeStamps) { addTimeStampJcheckBox.setSelected(addTimeStamps); }

  //get the file size in bytes
  public long getFileSize() { return (long)fileSizeJTextField.getNumber(); }
  public boolean getAppendToFile() { return appendJcheckBox.isSelected(); }
  public boolean getRollAround() { return rollAroundJcheckBox.isSelected(); }
  public boolean getAddTimeStamps() { return addTimeStampJcheckBox.isSelected(); }
  /**
   * Returns the file object selected of null if not selected
   */
  public File getSelectedFile()
  {
    return selectFileDialog.getSelectedFile();
  }
  //returns JFileChooser.APPROVE_OPTION if selected
  public int showSaveFileDialog(Frame frame) { return selectFileDialog.showSaveDialog(frame); }
  //returns JFileChooser.APPROVE_OPTION if selected
  public int showSaveDirDialog   (Frame frame,String title, boolean showFiles)
  {
    if( showFiles )
    {
      selectFileDialog.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);      
    }
    else
    {
      selectFileDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }
    selectFileDialog.setDialogTitle(title);
    return selectFileDialog.showSaveDialog(frame);
  }
}