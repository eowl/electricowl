/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.gui;

import javax.swing.*;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.LogAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import java.awt.*;

import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.util.LogFormat;
import org.jfree.data.Range;
import org.jfree.chart.renderer.xy.*;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.util.ShapeUtilities;

public class GenericPlotPanel extends JPanel 
{
  public static Color       PlotColours[] = {new Color(0,100,0) , Color.black, Color.green, Color.red, Color.blue, Color.cyan, Color.darkGray, Color.gray, Color.lightGray, Color.magenta, Color.orange, Color.pink, Color.yellow };

  public String    plotTitle="";
  public String    plotName="";
  public String    xAxisName="";
  public String    yAxisName="";
  public boolean   linesEnabled=true;
  public boolean   shapesEnabled=true;
  public boolean   autoScaleEnabled=true;
  public double    minScaleValue=0;
  public double    maxScaleValue=0;
  public long      maxAgeSeconds=3600;
  public boolean   logYAxis=false;
  public boolean   zeroOnXScale=true;
  public boolean   zeroOnYScale=true;
  public boolean   enableLegend=false;
  public int       yAxisTickCount=0;
  
  int yAxisIndex=0;
  
  public GenericPlotPanel(LayoutManager layoutManager)
  {
    super(layoutManager);
  }
  
  public String toString()
  {
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append("plotTitle          = "+plotTitle+"\n");
    strBuffer.append("plotName           = "+plotName+"\n");
    strBuffer.append("xAxisName          = "+xAxisName+"\n");
    strBuffer.append("yAxisName          = "+yAxisName+"\n");
    strBuffer.append("linesEnabled       = "+linesEnabled+"\n");
    strBuffer.append("shapesEnabled      = "+shapesEnabled+"\n");
    strBuffer.append("autoScaleEnabled   = "+autoScaleEnabled+"\n");
    strBuffer.append("minScaleValue      = "+minScaleValue+"\n");
    strBuffer.append("maxScaleValue      = "+maxScaleValue+"\n");
    strBuffer.append("maxAgeSeconds      = "+maxAgeSeconds+"\n");
    strBuffer.append("logYAxis           = "+logYAxis+"\n");
    return strBuffer.toString();
  }
  
  
  void genericConfig(JFreeChart chart, XYPlot plot, int plotIndex)
  {
    if( !enableLegend )
    {
      chart.removeLegend();
    }
    XYItemRenderer xyItemRenderer = plot.getRenderer();
    //May also be XYBarRenderer
    if( xyItemRenderer instanceof XYLineAndShapeRenderer ) {
      //If currently an XYLineAndShapeRenderer replace it so that we inc the colour for every plotIndex
      XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer(linesEnabled, shapesEnabled);
      //Set series colors
      renderer.setSeriesPaint(0, new Color(0,100,0) );
      renderer.setSeriesPaint(1, Color.RED );
      //Don't show lines on the plot used to indicate >= to max power limit
      renderer.setSeriesLinesVisible(1, false);
      renderer.setSeriesShape(1, ShapeUtilities.createDiamond(10.0f));

      renderer.setSeriesStroke(plotIndex, new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
      ((XYPlot)chart.getPlot()).setRenderer(plotIndex, renderer);
    }
    
    //If we have a new y axis then we need a new data set
    if( yAxisName != null && yAxisName.length() >  0 )
    {    
      if( logYAxis ) {
        LogAxis yAxis = new LogAxis(yAxisName);  
        yAxis.setAutoRange(false);
        yAxis.setNumberFormatOverride(new LogFormat(10, "10", true));
        yAxis.setRange(minScaleValue, maxScaleValue);
        yAxis.setLowerBound(minScaleValue);
        yAxis.setUpperBound(maxScaleValue);
        plot.setRangeAxis(yAxisIndex, yAxis);
        plot.setRangeAxisLocation(yAxisIndex, AxisLocation.BOTTOM_OR_LEFT);
      }
      else {
        NumberAxis axis = new NumberAxis(yAxisName);
        axis.setAutoRangeIncludesZero(zeroOnYScale);
        if( autoScaleEnabled )
        {
          axis.setAutoRange(true);
        }
        else
        {
          Range range = new Range(minScaleValue, maxScaleValue);
          axis.setRangeWithMargins(range, true, true);
        }     
        if ( yAxisTickCount > 0 ) {
          NumberTickUnit tick = new NumberTickUnit(yAxisTickCount);
          axis.setTickUnit(tick);        
        }
        plot.setRangeAxis(yAxisIndex, axis);
        plot.setRangeAxisLocation(yAxisIndex, AxisLocation.BOTTOM_OR_LEFT);
      }
      yAxisIndex++;    
    }
    plot.mapDatasetToRangeAxis(plotIndex,yAxisIndex-1);
    ValueAxis a = plot.getDomainAxis();
    if( xAxisName.length() > 0)
    {
      a.setLabel(xAxisName);
    }
    //We can enable/disable zero on the axis if we have a NumberAxis
    if( a instanceof NumberAxis )
    {
      ((NumberAxis)a).setAutoRangeIncludesZero(zeroOnXScale);
    }
  }
  
  Color getPlotColour(int plotIndex)
  {
    int colourIndex;
    if( plotIndex >= GenericPlotPanel.PlotColours.length )
    {
      colourIndex=plotIndex%GenericPlotPanel.PlotColours.length;
    }
    else
    {
      colourIndex=plotIndex;
    }
    return GenericPlotPanel.PlotColours[colourIndex];
  }


}