/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Date;

/**
 * Responsible for displaying the staus line (typically at the bottom of a frame)
 * Maybe double clicked to show the history.
 * @author Paul Austen
 */
public class StatusBar extends JPanel implements ActionListener, StatusBarInterface, MouseListener
{
  private JLabel statusBarLabel = new JLabel();
  private JFrame historyFrame;
  private ScrollableMessagePanel scrollableMessagePanel;
  public  static String ToolTip="Double click for message history"; 
  private Timer t;
  private boolean includeDateTime;
  
  /**
   * Constructor
   * 
   * @param includeDateTime if true then log messages will be prefixed by the date time string
   */
  public StatusBar(boolean includeDateTime)
  {
    this.includeDateTime=includeDateTime;
    this.setLayout(new FlowLayout(FlowLayout.LEFT));
    add(statusBarLabel);
    setBorder( BorderFactory.createLoweredBevelBorder());
    status(" ",false);
    
    historyFrame = new JFrame("Status message history");
    scrollableMessagePanel = new ScrollableMessagePanel(20,132,false);
    historyFrame.getContentPane().add(scrollableMessagePanel);
    historyFrame.pack();
    UI.CenterOnScreen(historyFrame);
    addMouseListener(this);
    statusBarLabel.addMouseListener(this);
    statusBarLabel.setToolTipText(ToolTip);
    this.setToolTipText(ToolTip);
  }
  /**
   * Constructor
   * 
   * Not date/time string will be added to the log
   */
  public StatusBar()
  {
    this(false);
  }
  
  /**
   * Return the data time Sting
   * @return
   */
  private String getDateTime()
  {
    if( includeDateTime )
    {
      return ""+new Date()+": ";
    }
    return "";
  }
  
  private void status(String line, boolean clearDown)
  {
    statusBarLabel.setText(line);
    if( scrollableMessagePanel != null )
    {
      scrollableMessagePanel.append(getDateTime()+line+"\n");
    }
    if( clearDown )
    {
      if( t == null )
      {
        t = new Timer(3000,this);
      }
      if( t.isRunning() )
      {
        t.restart();
      }
      else
      {
        t.restart();
      }
    }
  }
  
  public void println(String line)
  {
    status(line, true);
  }
  
  public void println_persistent(String line)
  {
    status(line, false);
  }
  
  /**
   * This is the same as println execpt it does not print the text in the status nbar.
   * It is added to the scrollable text for the user to view but is for text that you 
   * don't want to appesr on the status bar but do wish to appear in the log.
   * 
   * @param line The line3 of text
   */
  public void log(String line)
  {
    if( scrollableMessagePanel != null )
    {
      scrollableMessagePanel.append(getDateTime()+line+"\n");
    }
  }  
  
  //Called to clear down the last message
  public void actionPerformed(ActionEvent e) 
  {
    //Clear status text, keep space as this ensures that the vertical size of the status bar does not change.
    statusBarLabel.setText(" ");
  }
  
  public void mouseClicked(MouseEvent e) 
  {
    //If double click 
    if( e.getClickCount() == 2 )
    {
      //Display the history frame
      historyFrame.setVisible(true);
    }
  }
  public void mouseEntered(MouseEvent e) {}
  public void mouseExited(MouseEvent e) {}
  public void mousePressed(MouseEvent e) {}
  public void mouseReleased(MouseEvent e) {}


}
