/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * This allows rows of components to be displayed two on a row.
 * Typically this is a label on the left and a selectable component
 * (JTextField etc) on the right.
 * 
 */
public class RowPane extends JPanel
{
  private GridBagLayout gridBagLayout;
  private GridBagConstraints constraints;
  private int rowCount=0;
  
  public RowPane()
  {
    gridBagLayout = new GridBagLayout();
    setLayout(gridBagLayout);
    constraints = new GridBagConstraints();
    constraints.insets= new Insets(UI.VSPACE, UI.HSPACE, UI.VSPACE, UI.HSPACE);    
  }

  /**
   * Add a row to the RowPanel.
   * 
   * @param leftComponent
   *            The object to be displayed on the left hand side of the row.
   * @param rightComponent
   *            The object to be displayed on the right hand side of the row.
   */
  public void add(JComponent leftComponent, JComponent rightComponent)
  {
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.gridx = 0;
    constraints.gridy = rowCount;
    add(leftComponent, constraints);

    constraints.gridx = 1;
    constraints.gridy = rowCount;
    add(rightComponent, constraints);
    
    rowCount++;
  }

  /**
   * Add a component on a row
   * 
   * @param leftComponent
   */
  public void add(JComponent leftComponent)
  {
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.gridx = 0;
    constraints.gridy = rowCount;
    add(leftComponent, constraints);
    rowCount++;
  }
}
