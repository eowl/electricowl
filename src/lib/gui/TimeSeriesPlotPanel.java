/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.gui;

import java.awt.BorderLayout;
import java.awt.Color;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.*;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import java.util.*;
import java.text.*;

public class TimeSeriesPlotPanel extends GenericPlotPanel
{
  private JFreeChart                    chart;
  private ChartPanel                    chartPanel;
  private TimeSeries                    timeSeries;
  private TimeSeries                    redTimeSeries;

  
  public TimeSeriesPlotPanel() {
     super(new BorderLayout());
     init();
   }
     
   public void init()
   {
     finalize();
     chart = createChart(null);
     chartPanel = new ChartPanel(chart);
     chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
     add(chartPanel);
  }
   
   private JFreeChart createChart(XYDataset dataset) {
     
     JFreeChart chart = ChartFactory.createTimeSeriesChart(
         plotTitle,     // title
         xAxisName,     // x-axis label
         yAxisName,     // y-axis label
         dataset,       // data
         enableLegend,  // create legend?
         true,          // generate tooltips?
         true           // generate URLs?
     );
     
     XYPlot plot = (XYPlot) chart.getPlot();
     plot.setBackgroundPaint(Color.LIGHT_GRAY);
     ((XYPlot)plot).setDomainGridlinePaint(Color.DARK_GRAY);
     ((XYPlot)plot).setRangeGridlinePaint(Color.DARK_GRAY);
     plot.setDomainCrosshairVisible(true);
     plot.setRangeCrosshairVisible(true);
     DateAxis axis = (DateAxis) plot.getDomainAxis();
     axis.setDateFormatOverride(new SimpleDateFormat("HH:mm dd-MMM-yyyy"));
     
     return chart;
 }

  public void addPlot()
  {
    timeSeries = new TimeSeries(plotName, Second.class);
    //For this plot we only draw the points when setNotify is called
    timeSeries.setNotify(false);
    timeSeries.setMaximumItemAge(maxAgeSeconds);

    //This plot is used to highlight the high power usage 
    redTimeSeries = new TimeSeries(plotName, Second.class);
    //For this plot we only draw the points when setNotify is called
    redTimeSeries.setNotify(false);
    redTimeSeries.setMaximumItemAge(maxAgeSeconds);

    TimeSeriesCollection dataset = new TimeSeriesCollection();
    dataset.addSeries( timeSeries );
    dataset.addSeries( redTimeSeries );
    ((XYPlot)chart.getPlot()).setDataset(dataset);
    genericConfig(chart, (XYPlot)chart.getPlot(), 0);  
  }
  
  public int getPlotCount() { return ((XYPlot)chart.getPlot()).getSeriesCount(); }
   
  public void addPlotValue(double plotIndex, Date date, double yValue, boolean plotRedPoint)
  {  
	    timeSeries.addOrUpdate( new Second(date), yValue );
	    if( plotRedPoint ) {
	    	redTimeSeries.addOrUpdate( new Second(date), yValue );
	    }
  }
  
  public void removePlots()
  {
    init();
    validate();
  }
  
  public void finalize()
  {
    timeSeries = null;
    removeAll();
    chartPanel=null;
    chart=null;
  }
  
  public void setNotify(boolean notify)
  {
    timeSeries.setNotify(notify);
    timeSeries.fireSeriesChanged();
  }
}
