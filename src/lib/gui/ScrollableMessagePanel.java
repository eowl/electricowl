/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.gui;

import  java.io.*;
import  java.util.*;
import  java.awt.*;
import  javax.swing.*;
import  java.awt.event.*;
import  java.awt.datatransfer.*;
import  lib.io.*;
/**
 * Responsible for providing a scrollable text message area.
 * 
 * @author Paul Austen
 *
 */
public class ScrollableMessagePanel extends JScrollPane implements MouseListener, ActionListener
{
  public static final Color DEFAULT_BACKGROUND=Color.white;
  public static final Color DEFAULT_FOREGROUND=Color.black;
  //default to 250 ms moude down period in order to display the popup menu
  public static final int DEFAULT_POPUP_PERIOD=250;
  private SelectableTextArea dta;
  //object used to log text added to this text area into a log file
  private TextLogFile textLogFile;
  //default filter text is off
  private boolean	filterText;
  private boolean       okToUpdate=true;
  private JMenuItem     cutMenuItem;
  private JMenuItem     copyMenuItem;
  private JMenuItem     pasteMenuItem;
  private JMenuItem     selectMenuItem;
  private JMenuItem     saveToFileMenuItem;
  private JMenuItem     captureToFileMenuItem;
  private JMenuItem     cancelMenuItem;
  private JMenuItem     clearMenuItem;
  private TextLogFileDialog tlfd;
  private JFileChooser  jfc = new JFileChooser();
  private boolean       popupEnabled=true;
  private Frame         parentF=null;
  //list of usr configurable menu items
  private Vector        customMenuItems = new Vector(0,1);
  private long  mouseDownTime;
  private long popupPeriod=DEFAULT_POPUP_PERIOD;
  private int maxTextBufferSize=131072;

  /**
   * Constructor
   *
   */
  public ScrollableMessagePanel(boolean editable)
  {
    dta = new SelectableTextArea();
    dta.addMouseListener(this);
    dta.setEditable(editable);
    dta.setFont( new Font( "Monospaced" , Font.PLAIN ,12) );
    dta.setBackground(DEFAULT_BACKGROUND);
    dta.setForeground(DEFAULT_FOREGROUND);
    getViewport().add(dta, null);
  }
  public ScrollableMessagePanel()
  {
    //By default this constructor creates a ScrollableMessagePanel which is not editable.
    this(false);
  }
  
  /**
   * Constructor
   * 
   * @param row The number of rows in the text area.
   * @param col The number of colums in the text area.
   */
  public ScrollableMessagePanel(int row , int col, boolean editable)
  {
    dta = new SelectableTextArea(row,col);
    dta.addMouseListener(this);
    dta.setEditable(editable);
    dta.setFont( new Font( "Monospaced" , Font.PLAIN ,12) );
    dta.setBackground(DEFAULT_BACKGROUND);
    dta.setForeground(DEFAULT_FOREGROUND);
    dta.setCaretColor(DEFAULT_FOREGROUND);
    getViewport().add(dta, null);
  }
  public ScrollableMessagePanel(int row , int col)
  {
    //By default this constructor creates a ScrollableMessagePanel which is not editable.
    this(row, col, false);
  }
  
  /**
   * Add a line of text to the text area. 
   * A line feed character is added to the end of the line.
   * 
   * @param line The line of text to be added.
   */
  public void println(String line)
  {
    appendWithLimits(line,true);
  }
  
  /**
   * Add a line of text to the text area. 
   * A line feed character is added to the end of the line.
   * 
   * @param line The line of text to be added.
   * @param scrollToEnd If true, an attempt is made to scroll the end of the buffer.
   */
  public void println(String line,boolean scrollToEnd)
  {
    appendWithLimits(line,scrollToEnd);
  }
  

  /**
   * Append text to the text area. 
   * 
   * @param text The text to be added.
   */
  public synchronized void append(String text)
  {
    append(text,true);
  }
  
  /**
   * Append text to the text area. 
   * 
   * @param text The text to be added.
   * @param scrollToEnd If true, an attempt is made to scroll the end of the buffer.
   */
  public synchronized void append(String text,boolean scrollToEnd)
  {
    //if we don't want this updated then quit now
    if( !okToUpdate )
    {
      return;
    }
    appendWithLimits(text,scrollToEnd);
  }
  
  
  /**
   * Set the text in the text area. The text may not be longer than the max 
   * 
   * @param text The text.
   */
  public synchronized void setText(String text)
  {
    setText(text,true);
  }
  
  /**
   * Get the text displayed.
   * 
   * @return The displayed text.
   */
  public String getText() { return dta.getText(); }
  
  /**
   * Set the text in the text area. The text may not be longer than the max 
   * 
   * @param text The text.
   * @param scrollToEnd If true, an attempt is made to scroll the end of the buffer.
   */
  public synchronized void setText(String text,boolean scrollToEnd)
  {
    //if we don't want this updated then quit now
    if( !okToUpdate )
    {
      return;
    }
    if( text.length() <= maxTextBufferSize )
    {
      dta.setText(text);
      //set view the end of the buffer so that a scroll down occurs if required
      try
      {
        //If required to scroll to end
        if( scrollToEnd )
        {
          dta.setCaretPosition( dta.getText().length() );
        }
      }
      catch(Exception ex) {}
    }
  }
  
  /**
   * Append text to the text area. 
   * Apply a limitation to the size of text held in the text area.
   * 
   * @param text The text to be added.
   * @param scrollToEnd If true, an attempt is made to scroll the end of the buffer.
   */
  private	synchronized void	appendWithLimits(String text,boolean scrollToEnd)
  {
    //the amount of bytes that will be removed from the
    //textarea
    int	chopSize;
    //if invalid text then abort
    if( text == null || text.length() == 0 )
    {
      return;
    }
    //if required to log the data to a file
    //then do it
    if( textLogFile != null )
    {
     try
      {
        textLogFile.append(text);
      }
      catch(IOException ex)
      {
        System.out.println("" + textLogFile + " file append error. " + ex);
      }
    }
    //if required to filter text (i.e remove all control chars except line feed)
    if( filterText )
    {
      StringBuffer filterBuffer = new StringBuffer();
      for(int i=0;i<text.length();i++)
      {
        if( ( text.charAt(i) >= 32 && text.charAt(i) <= 127 ) ||
        text.charAt(i) == '\n' )
        {
          filterBuffer.append( text.charAt(i) );
        }
      }
      text = new String( filterBuffer );
    }
    //if required to remove some text from the text area
    if( (dta.getText().length() + text.length()) > maxTextBufferSize )
    {
      //if more chars to be added to buffer than there is space for
      if(text.length() > maxTextBufferSize)
      {
        //only add the last chunk of text that is big enough to fit into the buffer
        text=text.substring(text.length()-maxTextBufferSize,text.length());
      }
      //set default chop size to 1/3 of the textarea buffer
      chopSize=dta.getText().length()/3;
      //if the size of the string to be added is greater
      //than 1/3 of the current textarea buffer
      if( chopSize < text.length() )
      {
        //set the size of text to be removed = to the
        //amount of text that will be added
        chopSize=text.length();
        //if more text to be removed than is present
        //then only remove what is present
        if( chopSize > dta.getText().length() )
        {
          chopSize=dta.getText().length();
        }
      }
      //replace the range with the no text
      dta.replaceRange("",0,chopSize );
    }
    //now the buffer has been reduced to fit the text in
    //add the text to the end of the buffer
    dta.append(text);
    //set view the end of the buffer so that a scroll down occurs if required
    try
    {
      //If required to scroll to end
      if( scrollToEnd )
      {
        dta.setCaretPosition(dta.getDocument().getLength());
      }
    }
    catch(Exception ex) {}
  }
  
  /**
   * Set the filter text flag. 
   * 
   * @param filterText If true then all ASCII control characters except line feed will be removed.
   */
  public void setFilterText(boolean filterText) { this.filterText=filterText;}
  
   /**
   * Set a log file to capture text sent to this text area to and start file capture.
   * 
   * @param maxLogFileSize The max size of the log file in bytes.
   * @param header The header to write to the log file.
   * @param append If true the file will be appended to. If false it will be overwritten.
   * @param rollAround If true then when the max size is reached the older data will be lost.
   * @param addTimeStamps If true then each message added to the log file will be prefixed with a time stamp to denote when it was added to the log file.
   * @param timeStampPrefix If the above is true then this text will be added in front (to the left of) the time stamp.
   */
  public void setLogFile( File logFile , int maxLogFileSize , String header , boolean append , boolean rollAround , boolean addTimeStamps , String timeStampPrefix)
  {
    textLogFile = new TextLogFile(logFile,maxLogFileSize,header,append,rollAround,addTimeStamps,timeStampPrefix);
  }

  /**
   * Stop capturing text to the log file.
   * 
   */
  public void stopLogging()
  {
    textLogFile=null;
  }
  
  /**
   * Set the message area update state.
   * 
   * @param state If true then text can be added to the text area.
   */
  public void setUpdate(boolean state)
  {
    okToUpdate=state;
  }
  
  /**
   * Get the JTextArea object attribute.
   * 
   * @return The JTextArea object containing text held in this object.
   */
  public JTextArea getJTextArea() { return dta; }
  
  /**
   * Set the max buffer size in bytes.
   * 
   * @param maxTextBufferSize The max buffer size in bytes.
   */
  public void setMaxBufferSize(int maxTextBufferSize)
  {
    this.maxTextBufferSize=maxTextBufferSize;
  }

  
  public void mouseClicked(MouseEvent e)
  {
  }
  public void mousePressed(MouseEvent e)
  {
    mouseDownTime=System.currentTimeMillis();
  }
  public void mouseReleased(MouseEvent e)
  {
    //if the popup is enabled
    if( popupEnabled )
    {
      showPopupMenu( ( Component )e.getSource(), e.getX(), e.getY() );
    }
  }
  public void mouseEntered(MouseEvent e)
  {
  }
  public void mouseExited(MouseEvent e)
  {
  }

  
  /**
   * Display popup menu
   **/
  private void showPopupMenu( Component invoker, int x, int y )
  {
    long mouseDownPeriod=(System.currentTimeMillis()-mouseDownTime);
    //Mouse must be held down for at least popupPeriod before the menu will
    //appear
    if( mouseDownPeriod < popupPeriod )
    {
      return;
    }
    JPopupMenu popup = new JPopupMenu( "" );
    //If we have som customised popup menus
    if( customMenuItems.size() > 0 )
    {
      CustomMenuItem cmi;
      for(int i=0;i<customMenuItems.size();i++)
      {
        cmi = (CustomMenuItem)customMenuItems.elementAt(i);
        popup.add( cmi.getMenuItem() );
      }
      //Add a separator between these menu items and the standard ones
      popup.addSeparator();
    }

    /**
     * Set the menu items
     */
    cutMenuItem = new JMenuItem( "Cut" );
    cutMenuItem.setHorizontalTextPosition( SwingConstants.RIGHT );
    cutMenuItem.addActionListener(this);
    if( dta.isEditable() )
    {
      popup.add(cutMenuItem);
    }
    copyMenuItem = new JMenuItem( "Copy" );
    copyMenuItem.setHorizontalTextPosition( SwingConstants.RIGHT );
    copyMenuItem.addActionListener(this);
    popup.add(copyMenuItem);

    pasteMenuItem = new JMenuItem( "Paste" );
    pasteMenuItem.setHorizontalTextPosition( SwingConstants.RIGHT );
    pasteMenuItem.addActionListener(this);

    if( dta.isEditable() )
    {
      popup.add(pasteMenuItem);
    }

    //is all the text selected
    if( isAllSelected() )
    {
      selectMenuItem = new JMenuItem( "De-select all" );
    }
    else
    {
      selectMenuItem = new JMenuItem( "Select all" );
    }
    selectMenuItem.setHorizontalTextPosition( SwingConstants.RIGHT );
    selectMenuItem.addActionListener(this);
    popup.add( selectMenuItem );

    popup.addSeparator();

    saveToFileMenuItem = new JMenuItem( "Save to file" );
    saveToFileMenuItem.setHorizontalTextPosition( SwingConstants.RIGHT );
    saveToFileMenuItem.addActionListener(this);
    popup.add( saveToFileMenuItem );
    //if currently logging to a file
    if( textLogFile != null )
    {
      captureToFileMenuItem = new JMenuItem( "Turn capture off" );
      captureToFileMenuItem.setHorizontalTextPosition( SwingConstants.RIGHT );
      captureToFileMenuItem.addActionListener(this);
      popup.add( captureToFileMenuItem );
    }
    else
    {
      captureToFileMenuItem = new JMenuItem( "Turn capture on" );
      captureToFileMenuItem.setHorizontalTextPosition( SwingConstants.RIGHT );
      captureToFileMenuItem.addActionListener(this);
      popup.add( captureToFileMenuItem );
    }

    popup.addSeparator();

    clearMenuItem = new JMenuItem( "Clear" );
    clearMenuItem.setHorizontalTextPosition( SwingConstants.RIGHT );
    clearMenuItem.addActionListener(this);
    popup.add(clearMenuItem);

    popup.addSeparator();

    cancelMenuItem = new JMenuItem( "Cancel" );
    cancelMenuItem.setHorizontalTextPosition( SwingConstants.RIGHT );
    cancelMenuItem.addActionListener(this);
    popup.add( cancelMenuItem );

    popup.show( invoker, x, y );
  }

  /**
   * Enable the popup menu.
   * 
   * @param popupEnabled If true then the popup menu will be displayed.
   */
  public void enabledPopup(boolean popupEnabled)
  {
    this.popupEnabled=popupEnabled;
  }

  /**
   * Return true if all the text is selected
   */
  private boolean isAllSelected()
  {
    //if all the text is selected
    if( dta.getText() != null && dta.getSelectedText() != null && dta.getSelectedText().length() == dta.getText().length() )
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * process popup menu action
   */
  public void actionPerformed(ActionEvent e)
  {
    if( e.getSource() == cutMenuItem )
    {
      if( dta.getSelectedText() == null || dta.getSelectedText().length() == 0 )
      {
        loadParentFrame();
        Dialogs.showErrorDialog(parentF,"Error","No text is selected to cut.");
        return;
      }
      else
      {
        dta.cut();
      }
    }
    if( e.getSource() == copyMenuItem )
    {
      if( dta.getSelectedText() == null || dta.getSelectedText().length() == 0 )
      {
        loadParentFrame();
        Dialogs.showErrorDialog(parentF,"Error","No text is selected to copy.");
        return;
      }
      else
      {
        dta.copy();
      }
    }
    if( e.getSource() == pasteMenuItem )
    {
      Clipboard clipboard = getToolkit().getSystemClipboard();
      Transferable clipData = clipboard.getContents(this);
      String s="";
      try {
        s = (String)(clipData.getTransferData(DataFlavor.stringFlavor));
      }
      catch (Exception ex) {}
      dta.insert(s,dta.getCaretPosition());
    }
    if( e.getSource() == selectMenuItem )
    {
      //if all the text selected
      if( isAllSelected() )
      {
        //deselect all
        dta.select(0,0);
        //set the cursor position to the end of the
        dta.setCaretPosition(dta.getText().length()-1);
      }
      else
      {
        //select all
        dta.selectAll();
      }
    }
    if( e.getSource() == saveToFileMenuItem )
    {
      if( dta.getSelectedText() == null || dta.getSelectedText().length() == 0 )
      {
        loadParentFrame();
        Dialogs.showErrorDialog(parentF,"Error","No text is selected to save.");
        return;
      }
      //if this is the first use of the file chooser then create it
      if( jfc == null )
      {
        jfc = new JFileChooser();
      }
      jfc.rescanCurrentDirectory();
      int response = jfc.showSaveDialog(this);
      //if the user selected a file
      if( response == JFileChooser.APPROVE_OPTION && jfc.getSelectedFile() != null )
      {
        //if the file already exists
        if( jfc.getSelectedFile().isFile() )
        {
          response = Dialogs.showYesNoDialog(null,"Warning","The " + jfc.getSelectedFile().getName() + "exists.\nDo you wish to overwrite this file ?");
          if( response != JOptionPane.YES_OPTION )
          {
            return;
          }
          //delete the file
          jfc.getSelectedFile().delete();
          //if the file is not deleted
          if( jfc.getSelectedFile().isFile() )
          {
            Dialogs.showErrorDialog(null,"Error","Unable to delete the " + jfc.getSelectedFile() + " file.");
            return;
          }
        }
        BufferedOutputStream bos = null;
        try
        {
          bos = new BufferedOutputStream( new FileOutputStream(jfc.getSelectedFile()) );
          bos.write(dta.getSelectedText().getBytes());
        }
        catch(IOException ex)
        {
          Dialogs.showErrorDialog(null,"Error",ex.getMessage());
        }
        finally
        {
          try
          {
            bos.close();
          }
          catch(Exception ex) {}
        }
      }
    }
    if( e.getSource() == captureToFileMenuItem )
    {
      //if on then turn it off
      if( textLogFile != null )
      {
        stopLogging();
      }
      else
      {
        //if this is the first use of the file chooser then create it
        if( tlfd == null )
        {
          tlfd = new TextLogFileDialog();
        }
        tlfd.rescanCurrentDirectory();
        loadParentFrame();
        //show the dialog
        int response = tlfd.showSaveFileDialog(parentF);
        if( response == JFileChooser.APPROVE_OPTION && tlfd.getSelectedFile() != null )
        {
          //if user does not want to append to the file and the file exists
          if( !tlfd.getAppendToFile() && tlfd.getSelectedFile().isFile() )
          {
            //if not ok to overwrite the file
            if( !FileIO.OkToWriteFile(parentF,tlfd.getSelectedFile()) )
            {
              return;
            }
          }
          //create a logfile
          textLogFile = new TextLogFile(tlfd.getSelectedFile(),(int)tlfd.getFileSize(),"Start Capture",tlfd.getAppendToFile(),tlfd.getRollAround(),tlfd.getAddTimeStamps(),"");
        }
      }
    }
    if( e.getSource() == clearMenuItem )
    {
      dta.setText("");
    }
    if( e.getSource() == cancelMenuItem )
    {
    }
  }

  /**
   * Add a custom popup menu item
   * 
   * @param customMenuItem The custom menu item to be added to the menu.
   */
  public void addCustomMenuItem(CustomMenuItem customMenuItem)
  {
    customMenuItems.addElement(customMenuItem);
  }

  /**
   * Remove a custom menu item
   * 
   * @param customMenuItem The custom menu item already in the menu that is to be removed.
   */
  public void removeCustomMenuItem(CustomMenuItem customMenuItem)
  {
    customMenuItems.removeElement(customMenuItem);
  }

  /**
   * Load the parent Frame
   */
  private void loadParentFrame()
  {
    if( parentF == null )
    {
      try
      {
        parentF=UI.GetParentFrame(this);
      }
      catch(Exception ex) {}
      if( parentF == null )
      {
        parentF=new Frame();
      }
    }
  }

  /**
   * Set popupPeriod
   */
  public void setPopupPeriod(long popupPeriod) { this.popupPeriod=popupPeriod; }

  /**
   * Get popupPeriod
   */
  public long getPopupPeriod() { return popupPeriod; }
  
  /**
   * Set the number of rows for the scrollable text area.
   * 
   * @param rows The number of rows
   */
  public void setRows(int rows)
  {
    dta.setRows(rows);
  }

  /**
   * Set the number of columns in the scrollable text area.
   * @param cols
   */
  public void setColumns(int cols)
  {
    dta.setColumns(cols);
  }

}


//subclass textarea because the select function does not work unless the
//textarea is in focus. The EditTextdialog has a gotot line button that
//highlights the required line via a button at the bottom of the dialog.
//Adding getCaret().setSelectionVisible(true); to the overrided
//moveCaretPosition method achieves this
/**
 * Extend textarea because the select function does not work unless the
 * textarea is in focus. The EditTextdialog has a gotot line button that
 * highlights the required line via a button at the bottom of the dialog.
 * Adding getCaret().setSelectionVisible(true); to the overrided
 * moveCaretPosition method achieves this
 * 
 * @author Paul Austen
 *
 */
class SelectableTextArea extends JTextArea
{
  public  SelectableTextArea()
  {
    super();
  }
  public SelectableTextArea(int x,int y)
  {
    super(x,y);
  }
  public void moveCaretPosition(int pos)
  {
    super.moveCaretPosition(pos);
    getCaret().setSelectionVisible(true);
  }
}
