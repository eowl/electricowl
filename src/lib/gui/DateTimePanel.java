/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.gui;

import javax.swing.*;
import java.awt.Color;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.border.*; 
import java.text.*;
import java.awt.event.*;

public class DateTimePanel extends JPanel implements ActionListener
{
  public static final String MONTHS[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
  
  private JComboBox hoursField;
  private JComboBox minsField;
  private JComboBox dayField;
  private JComboBox monthField;
  private JComboBox yearField;
  private Vector<ActionListener>actionListeners;
  
  public DateTimePanel(String title)
  {
    setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY) ,title, TitledBorder.LEFT, TitledBorder.BELOW_TOP));
    minsField = new JComboBox();
    for( int i=0 ; i <=59 ; i++ )
    {
      minsField.addItem( new Integer(i) );
    }
    hoursField = new JComboBox();
    for( int i=0 ; i <=23 ; i++ )
    {
      hoursField.addItem( new Integer(i) );
    }
    dayField = new JComboBox();
    for( int i=1 ; i <=31 ; i++ )
    {
      dayField.addItem( new Integer(i) );
    }
    monthField = new JComboBox(MONTHS);
    yearField = new JComboBox();
    for( int i=2000 ; i <=2100 ; i++ )
    {
      yearField.addItem( new Integer(i) );
    }
    add( new JLabel("Time: ") );
    add( hoursField );
    add( minsField );
    add( new JLabel("     Date: ") );
    add(dayField);
    add(monthField);
    add(yearField);
    
    minsField.addActionListener(this);
    hoursField.addActionListener(this);
    dayField.addActionListener(this);
    monthField.addActionListener(this);
    yearField.addActionListener(this);
    
    actionListeners = new Vector<ActionListener>();
  }

  /**
   * Enabled disable the panel
   * 
   */
  public void setEnabled(boolean enabled)
  {
    hoursField.setEnabled(enabled);
    minsField.setEnabled(enabled);
    dayField.setEnabled(enabled);
    monthField.setEnabled(enabled);
    yearField.setEnabled(enabled);
  }
  
  /**
   * Get a Calendar object representing the date/time in the panel
   * 
   * @return
   */
  public Calendar getCalendar()
  {
    String dateString = yearField.getSelectedItem().toString()+"-"
    +monthField.getSelectedItem().toString()+"-"
    +dayField.getSelectedItem().toString()+" "
    +hoursField.getSelectedItem().toString()+":"
    +minsField.getSelectedItem().toString();
    
    DateFormat df = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
    Calendar cal=Calendar.getInstance();
    try
    {
      Date date = df.parse( dateString );
      cal.setTime(date);
    }
    catch(ParseException e) 
    {
      e.printStackTrace();
    } 
    return cal;
  }
  
  /**
   * Set the calendar date/time
   * 
   * @param calendar
   */
  public void setCalendar(Calendar calendar)
  {
    //We only want action events generated when the user select a control so disable them when the date is set programtically
    minsField.removeActionListener(this);
    hoursField.removeActionListener(this);
    dayField.removeActionListener(this);
    monthField.removeActionListener(this);
    yearField.removeActionListener(this);
    
    minsField.setSelectedItem( calendar.get(Calendar.MINUTE) );
    hoursField.setSelectedItem( calendar.get(Calendar.HOUR_OF_DAY) );
    dayField.setSelectedItem( calendar.get(Calendar.DAY_OF_MONTH) );
    monthField.setSelectedIndex( calendar.get(Calendar.MONTH) );
    yearField.setSelectedItem( calendar.get(Calendar.YEAR) );
        
    minsField.addActionListener(this);
    hoursField.addActionListener(this);
    dayField.addActionListener(this);
    monthField.addActionListener(this);
    yearField.addActionListener(this);
    
  }
  
  public void actionPerformed(ActionEvent e)
  {
    for( ActionListener actionListener : actionListeners )
    {
      actionListener.actionPerformed( new ActionEvent(this, e.getID(), e.getActionCommand()) );
    }
  }

  /**
   * Add an action listener
   * 
   * @param actionListener Will be called whenever a change is made to a date/time
   */
  public void addActionListener(ActionListener actionListener)
  {
    actionListeners.add(actionListener);
  }
  
  public void removeActionListener(ActionListener actionListener)
  {
    actionListeners.remove(actionListener);
  }
  
}

