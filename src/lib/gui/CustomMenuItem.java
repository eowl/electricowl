/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.gui;

import  javax.swing.*;
import  java.lang.reflect.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Place holder class for a customised popup menu item , its associated object
 * and the name of the method to execute if the menuitem is selected.
 * 
 * @author Paul Austen
 */
public class CustomMenuItem implements ActionListener
{
  private JMenuItem menuItem;
  private Object    execObject;
  private String    execMethodName;

  /**
   * Construct the custom menu item.
   * 
   * @param menuText The text to appear in the menu.
   * @param execObject The object which contains the method to be executed.
   * @param execMethodName The name of the method to be executed.
   */
  public CustomMenuItem(String menuText,Object execObject,String execMethodName)
  {
    menuItem = new JMenuItem( menuText );
    menuItem.setHorizontalTextPosition( SwingConstants.RIGHT );
    menuItem.addActionListener(this);
    setExecObject(execObject);
    setExecMethodName(execMethodName);
  }
  
  /**
   * Set the text to appear in the popup menu.
   * 
   * @param menuText The menu text.
   */
  public void setMenuText(String menuText)
  {
    menuItem.setText(menuText);
  }
  
  /**
   * Set the object on which the method may be executed when the menu option is selected.
   * 
   * @param execObject The object that contains the method.
   */
  public void setExecObject(Object execObject) { this.execObject=execObject; }
  
  /**
   * Set the name of the method to execute on the execObject when this menu option is selected.
   * 
   * @param execMethodName The name of the method.
   */
  public void setExecMethodName(String execMethodName) { this.execMethodName=execMethodName; }
  
  /**
   * Get the text to appear in the popup menu.
   * @return The menu text.
   */
  public String getMenuText() { return menuItem.getText(); }
  
  /**
   * Get the object on which the method may be executed when the menu option is selected.
   * 
   * @return The object that contains the method.
   */
  public Object getExecObjectName() { return execObject; }
  
  /**
   * Get the name of the method to execute on the execObject when this menu option is selected.
   * 
   * @return The name of the method.
   */
  public String getExecMethodName() { return execMethodName; }
  
  /**
   * Get the menuItem held in this object. This may then be added to the popup menu.
   * 
   * @return The JMenuItem
   */
  public JMenuItem getMenuItem() { return menuItem; }

  /**
   * Called when the menuItem is selected in order to execute the require method.
   */
  public void actionPerformed(ActionEvent e)
  {
    try
    {
      Method stepMethod;
      //get reference to the class containing the zero argument method
      Class c = execObject.getClass();
      //get a reference to the method object
      stepMethod = c.getMethod(execMethodName, (Class[])null );
      //invoke the method
      stepMethod.invoke(execObject, (Object[])null);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
}