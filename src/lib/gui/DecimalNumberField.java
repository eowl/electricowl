/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.gui;

/**
 * An entry field to allow the user to enter decimal numbers.
 *  
 * @author Paul Austen
 * @see LongNumberField
 */
public class DecimalNumberField extends LongNumberField
{
	/**
	 * Parameterless DecimalNumberField constructor
	 */
  public DecimalNumberField()
  {
		super(10);
  }
  
  /**
   * DecimalNumberField constructor with limits.
   * 
   * @param lowLimit The lowest valid value that may be entered 
   * into the DecimalNumberField.
   * @param highLimit The lowest valid value that may be entered 
   * into the DecimalNumberField.
   */
	public DecimalNumberField(long lowLimit, long highLimit)
	{
	  super(10,lowLimit,highLimit);
	}
 
}
