/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package lib.io;

import java.io.*;
import java.awt.*;
import javax.swing.*;
import lib.gui.*;
import java.util.*;

/**
 * Class responsible for providing useful File IO methods
 * 
 * @author Paul Austen
 */
public class FileIO
{
  /**
   * Default mechanism for overwriting a file. The user will be 
   * presented with dialogs (e.g do you wish to overwrite file) 
   * to ensure that they wish to delete the file in question.
   * 
   * @param frame The parent frame for dialogs
   * @param theFile The file to be deleted.
   * @return True if the user wishes to delete the file or the file does 
   * not exist.
   */
  public static boolean OkToWriteFile(Frame frame,File theFile)
  {
    boolean okToWriteFile=false;
    boolean failedToEraseFile=false;
    //if the file exists
    if( theFile.isFile() )
    {
      //if it is possible to write to this file
      if( theFile.canWrite() )
      {
        int response = Dialogs.showYesNoDialog(frame,"Warning",theFile.getAbsolutePath() + " file exists.\nDo you wish to overwrite this file ?");
        if( response == JOptionPane.YES_OPTION )
        {
          //delete the file
          theFile.delete();
          //if the file has been deleted
          if( !theFile.isFile() )
          {
            okToWriteFile=true;
          }
          else
          {
            failedToEraseFile=true;
          }
        }
        else
        {
          response = Dialogs.showYesNoDialog(frame,"Warning",theFile.getAbsolutePath() + " file exists.\nDo you wish to append to this file ?");
          if( response == JOptionPane.YES_OPTION )
          {
            okToWriteFile=true;
          }
        }
      }
      else
      {
        failedToEraseFile=true;
      }
      //if unable to erase the file
      if( failedToEraseFile )
      {
        Dialogs.showErrorDialog(frame,"Error","Failed to delete the " + theFile.getAbsolutePath() + " file.");
      }
    }
    //as file does not exist set flag as ok to write to file
    else
    {
      okToWriteFile=true;
    }
    return okToWriteFile;
  }
  
  /**
   * Read a properties file.
   * 
   * @param filename
   * @return
   */
  public static Properties GetProperties(File file) throws IOException
  {
	InputStream is=null;
	Properties properties = new Properties();
	try
	{
		is = new FileInputStream(file);
		properties.load(is);
	}
	finally
	{
	  try
	  {
	    is.close();
	  }
	  catch(Exception e) {}
	}	  
	return properties;
  }
  
  /**
   * Save a properties file
   * 
   * @param properties
   * @param filename
   * @param comments
   * @throws IOException
   */
  public static void SetProperties(Properties properties, File file, String comments) throws IOException
  {
	OutputStream os = null;
	try
	{
	  os = new FileOutputStream(file);
	  properties.store(os, comments);
	}
	finally
	{
	  try
	  {
	    os.close();
	  }
	  catch(Exception e) {}
	}
  }
  
  /**
   * Read all lines of text from the given file.
   * 
   * @param filename The filename to read lines of text from.
   * @return A Vector of Strings
   * @throws IOException
   */
  public static Vector<String> GetLines(String filename) throws IOException
  {
    Vector<String> lines = new Vector<String>(0,1);
    String line;
    BufferedReader br=null; 
    try
    {
      br = new BufferedReader(new FileReader(filename));
      //Read all the lines
      while(true)
      {
        //Read the current line
        line=br.readLine();
        if( line == null )
        {
          break;
        }
        //Add to the list of lines
        lines.addElement(line);
      }
    }
    finally
    {
      try
      {
        br.close();
      }
      catch(Exception e) {}
    }
    return lines;
  }

  /**
   * Save lines of text to the given file.
   * 
   * @param filename The filename to write lines of text to.
   * @param lines Lines of Text to write.
   * @param append If true append to the file
   * @throws IOException
   */
  public static void SetLines(String filename, Vector<String> lines,boolean append) throws IOException
  {
    BufferedWriter bw=null; 
    try
    {
      //Open the file
      bw = new BufferedWriter(new FileWriter(filename,append));
      //Write all the lines of text to the file
      for( String line : lines)
      {
        //Write lines to the file
        bw.write(line);
        //Write a line separator
        bw.newLine();
      }
    }
    finally
    {
      try
      {
        bw.close();
      }
      catch(Exception e) {}
    }
  }  
  
}