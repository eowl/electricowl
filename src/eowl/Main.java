/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package eowl;

import eowl.controller.ElectricOWLController;

public class Main
{
  public static void main(String args[])
  {
    ElectricOWLController electricOWLController = new ElectricOWLController();
    electricOWLController.start();
  }
}
