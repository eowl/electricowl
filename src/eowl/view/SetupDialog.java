/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package eowl.view;

import lib.gui.*;
import lib.shell.Executioner;

import javax.swing.*;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import eowl.model.*;

public class SetupDialog extends JDialog implements ActionListener, ItemListener
{
  private ElectricOWLFrame					electricOWLFrame;
  private boolean 							okSelected;
  private JPanel 							buttonPanel = new JPanel();
  private JButton 							okButton = new JButton("OK");
  private JButton 							cancelButton = new JButton("Cancel");
  private RowPane 							rowPane = new RowPane();
  private JRadioButton 						showAmpsRadioButton = new JRadioButton();
  private JRadioButton 						showPowerRadioButton = new JRadioButton();
  private JRadioButton 						showCostRadioButton = new JRadioButton();
  private DoubleNumberField 				costField = new DoubleNumberField();
  private DoubleNumberField 				voltageField = new DoubleNumberField();
  private JLabel 							costLabel = new JLabel();
  private JComboBox 						lookAndFeelField = new JComboBox();
  private int 								lastLookAndFeel;
  public static UIManager.LookAndFeelInfo[] LookAndFeels = UIManager.getInstalledLookAndFeels();
  private JRadioButton 						runLocalOWLServerButton  = new JRadioButton();
  private JTextField 						serverAddressField = new JTextField(20);
  private DecimalNumberField 				serverPortField = new DecimalNumberField(1,65535);
  private JTextField 						currencyField = new JTextField(5);
  private FilePanel 						logFilePanel = new FilePanel();
  private JRadioButton 						maxPowerTriggerRadioButton  = new JRadioButton("");
  private DoubleNumberField  				maxPowerTriggerValueKWH;
  private JTextField   						maxPowerTriggerShellCommand;
  private JButton 	   						testExternalCommandButton = new JButton("Test shell command");
  private DecimalNumberField 				maxPlotAgeSecField = new DecimalNumberField(10,60*60);
  
  public SetupDialog(ElectricOWLFrame electricOWLFrame)
  {
    super(electricOWLFrame, "Elctric OWL setup", true);
    this.electricOWLFrame=electricOWLFrame;
    buttonPanel.add(okButton);
    buttonPanel.add(cancelButton);
    add(buttonPanel, BorderLayout.SOUTH);
    okButton.addActionListener(this);
    cancelButton.addActionListener(this);
    showAmpsRadioButton.setToolTipText("When selected the Amps plot will be displayed for each sensor.");
    rowPane.add( new JLabel("Show Amps"), showAmpsRadioButton);
    
    showPowerRadioButton.setToolTipText("When selected the KW plot will be displayed for each sensor.");
    rowPane.add( new JLabel("Show Power"), showPowerRadioButton);
    
    showPowerRadioButton.setToolTipText("When selected the Cost plot will be displayed for each sensor.");
    rowPane.add( new JLabel("Show Cost"), showCostRadioButton);
    
    maxPlotAgeSecField.setToolTipText("Set the max plot age in seconds. The larger this value the more RAM the program will use (default = 600 )");
    rowPane.add( new JLabel("MAX plot age (seconds)"), maxPlotAgeSecField);
    
    voltageField.setToolTipText("The AC voltage (default 230V). A more accurate value, will give a more accurate power measurement.");
    rowPane.add( new JLabel("Voltage (V)"), voltageField);
    
    costField.setToolTipText("The cost of electtricity to you in currency units per kWh.");
    rowPane.add( costLabel, costField);
    
    currencyField.setToolTipText("The currency (default �)");
    rowPane.add( new JLabel("Currency"), currencyField);
    
    logFilePanel.setToolTipText("The log file int which to save your energy usage.");
    rowPane.add( new JLabel("Logging File"), logFilePanel);
    
    //Not thought to be a very useful option. The OwlServer should be left to run 
    //and the GUI can collect data from it when required.
    runLocalOWLServerButton.setSelected(false);
    
    //rowPane.add( new JLabel("Start OWL server"), runLocalOWLServerButton);
    serverAddressField.setToolTipText("The IP address/name of the computer running the OWL server (default=127.0.0.1, I.E this computer)");
    rowPane.add( new JLabel("OWL server address"), serverAddressField);
    
    serverPortField.setToolTipText("The TCP/IP port on which to connect to the OWL Server (default=12745)");
    rowPane.add( new JLabel("OWL server port"), serverPortField);

    maxPowerTriggerRadioButton.setToolTipText("If select then you can set a power level to highlight high power usage and optionally trigger an external command.");
    maxPowerTriggerRadioButton.addItemListener(this);
    rowPane.add(maxPowerTriggerRadioButton);
    rowPane.add( new JLabel("MAX Power Trigger"), maxPowerTriggerRadioButton);
    
    maxPowerTriggerValueKWH = new DoubleNumberField();
    maxPowerTriggerValueKWH.setToolTipText("The max power level in KW");
    
    maxPowerTriggerShellCommand = new JTextField(10);
    maxPowerTriggerShellCommand.setToolTipText("A command to be executed on this computer if the power level is at or greater than the maximum (optional)");
    rowPane.add( new JLabel("Trigger (KW)"), maxPowerTriggerValueKWH);
    
    rowPane.add( new JLabel("External shell command"), maxPowerTriggerShellCommand);
    rowPane.add( new JLabel(""), testExternalCommandButton);
    testExternalCommandButton.addActionListener(this);

    lookAndFeelField.setToolTipText("Select the GUI look and feel");
    rowPane.add( new JLabel("Look and Feel"), lookAndFeelField);
    
    runLocalOWLServerButton.addActionListener(this);
    add(rowPane, BorderLayout.CENTER);
    pack();
    setSize((int)(getWidth()*1.2), getHeight() );
    updateView();
  }
  
  private void updateView()
  {
    serverAddressField.setEnabled(!runLocalOWLServerButton.isSelected());
    serverPortField.setEnabled(!runLocalOWLServerButton.isSelected());
    maxPowerTriggerValueKWH.setEnabled(maxPowerTriggerRadioButton.isSelected());
    maxPowerTriggerShellCommand.setEnabled(maxPowerTriggerRadioButton.isSelected());
    testExternalCommandButton.setEnabled(maxPowerTriggerRadioButton.isSelected());    
  }
  
  public void setVisible(boolean visible)
  {
    okSelected=false;
    super.setVisible(visible);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    if( e.getSource() == okButton )
    {
      okSelected=true;
      super.setVisible(false);
    }
    else if( e.getSource() == cancelButton )
    {
      okSelected=false;
      super.setVisible(false);
    }
    else if( e.getSource() == runLocalOWLServerButton )
    {
      updateView();
    }
    else if (e.getSource() == testExternalCommandButton ) {
    	if( maxPowerTriggerShellCommand.getText().length() > 0 ) {
	    	try
	    	{
	    		new Executioner(electricOWLFrame, "", new SensorData(), getControllerConfig(new ControllerConfig()) ).start();
	    	}
	    	catch(SensorDataFormatException ex) 
	    	{
	    		ex.printStackTrace();
	    	}
    	}
    	else {
    		Dialogs.showErrorDialog(electricOWLFrame, "Error", "You must enter a shell command to test.");
    	}
    	
    }
  }
  
  public void itemStateChanged(ItemEvent e) {
    updateView();
  }
  
  public boolean OKSelected()
  {
    return okSelected;
  }
  
  public void setControllerConfig(ControllerConfig controllerConfig)
  {
    showAmpsRadioButton.setSelected(controllerConfig.viewAmps);
    showPowerRadioButton.setSelected(controllerConfig.viewkWh);
    showCostRadioButton.setSelected(controllerConfig.viewCost);
    costField.setNumber(controllerConfig.costPerkWh);
    costLabel.setText("Cost per kWh ("+controllerConfig.currencyString+")");
    voltageField.setNumber(controllerConfig.voltage);
    
    lookAndFeelField.removeAllItems();
    for (int i = 0; i < SetupDialog.LookAndFeels.length; i++)
    {
      lookAndFeelField.addItem(SetupDialog.LookAndFeels[i].getName());
    }
    lookAndFeelField.setSelectedIndex(controllerConfig.lookAndFeelIndex);
    lastLookAndFeel=controllerConfig.lookAndFeelIndex;
    runLocalOWLServerButton.setSelected(controllerConfig.runLocalOWLServer);
    serverAddressField.setText(controllerConfig.serverAddress);
    serverPortField.setNumber(controllerConfig.serverPort);
    currencyField.setText(controllerConfig.currencyString);
    logFilePanel.setSelectedFile(controllerConfig.logFile.getAbsolutePath());
    maxPowerTriggerRadioButton.setSelected(controllerConfig.maxPowerTriggerEnabled);
    maxPowerTriggerValueKWH.setNumber(controllerConfig.maxPowerTriggerKWh);
    maxPowerTriggerShellCommand.setText(controllerConfig.maxPowerExtCmd);
    maxPlotAgeSecField.setNumber(controllerConfig.maxPlotAgeSecs);
    updateView();
  }
  
  public ControllerConfig getControllerConfig(ControllerConfig controllerConfig)
  {
    controllerConfig.viewAmps=showAmpsRadioButton.isSelected();
    controllerConfig.viewkWh=showPowerRadioButton.isSelected();
    controllerConfig.viewCost=showCostRadioButton.isSelected();
    if( !controllerConfig.viewAmps && !controllerConfig.viewkWh && !controllerConfig.viewCost )
    {
      controllerConfig.viewkWh=true;
      Dialogs.showErrorDialog(electricOWLFrame, "Error", "You must select at least 1 plot, kW will be displayed.");
    }
    controllerConfig.costPerkWh=costField.getNumber();
    if( costField.getNumber() < 0 )
    {
      Dialogs.showErrorDialog(electricOWLFrame, "Error", "You entered a cost per kWh less than 0. Will use 0.0001");
      controllerConfig.costPerkWh=0.0001;
    }
    if( costField.getNumber() == 0 )
    {
      Dialogs.showErrorDialog(electricOWLFrame, "Error", "You entered a cost per kWh of 0. Will use 0.0001");
      controllerConfig.costPerkWh=0.0001;
    }
    controllerConfig.voltage=voltageField.getNumber();
    controllerConfig.lookAndFeelIndex = lookAndFeelField.getSelectedIndex();
    if( lastLookAndFeel != controllerConfig.lookAndFeelIndex )
    {
      Dialogs.showOKDialog(electricOWLFrame, "Look and Feel", "The "+SetupDialog.LookAndFeels[controllerConfig.lookAndFeelIndex].getName()+" look and feel will be used the next time you restart this program");
    }
    controllerConfig.runLocalOWLServer=runLocalOWLServerButton.isSelected();
    if( !controllerConfig.runLocalOWLServer )
    {
      //As these are not used when running a local OWL server
      //we only need copy them when not running a local server.
      controllerConfig.serverAddress=serverAddressField.getText();
      controllerConfig.serverPort=(int)serverPortField.getNumber();
    }
    controllerConfig.currencyString=currencyField.getText();
    controllerConfig.logFile=new File( logFilePanel.getSelectedFile() );
    
    if( maxPowerTriggerRadioButton.isSelected() ) {
    	if( maxPowerTriggerValueKWH.getNumber() == 0.0 )	{
    		controllerConfig.maxPowerTriggerEnabled=false;    		
    		controllerConfig.maxPowerTriggerKWh=15.0;    		
    	    Dialogs.showErrorDialog(electricOWLFrame, "Error", "When max power trigger is enabled the trigger value must be greater than 0");
    	}
    	else {
    		controllerConfig.maxPowerTriggerEnabled=true;   
    		controllerConfig.maxPowerTriggerKWh=maxPowerTriggerValueKWH.getNumber();
    		controllerConfig.maxPowerExtCmd=maxPowerTriggerShellCommand.getText();
    	}
    }
    else {
    	controllerConfig.maxPowerTriggerEnabled=false;    		
    }
    controllerConfig.maxPlotAgeSecs=maxPlotAgeSecField.getNumber();
    return controllerConfig;
  }
}
