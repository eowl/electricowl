/*
 * InputSensorNameDialog.java Created on 14 Mar 2010 05:41:32
 * 
 */
package eowl.view;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.Timer;
import lib.gui.UI;

public class InputSensorNameDialog extends JDialog implements ActionListener
{
  private ElectricOWLFrame  electricOWLFrame;
  private JPanel            northPanel = new JPanel();
  private JPanel            centerPanel = new JPanel();
  private JPanel            southPanel = new JPanel();
  private JButton           okButton = new JButton("OK");
  private JLabel            sensorNameLabel = new JLabel("");
  private JTextField        sensorNameField = new JTextField(30);
  private Timer             timer;
  private long              sensorDetectedTimeout=-1;
  
  public InputSensorNameDialog(ElectricOWLFrame electricOWLFrame)
  {
    super(electricOWLFrame, "", true);
    this.electricOWLFrame=electricOWLFrame;
    southPanel.add(okButton);
    okButton.addActionListener(this);
    northPanel.add(sensorNameLabel);
    centerPanel.add(sensorNameField);
    getContentPane().add(northPanel, BorderLayout.NORTH);
    getContentPane().add(centerPanel, BorderLayout.CENTER);
    getContentPane().add(southPanel, BorderLayout.SOUTH);
    pack();
    UI.CenterInParent(electricOWLFrame, this);
  }

  public void setSensorAddress(int sensorAddress)
  {
    sensorNameField.setText("Mains"+sensorAddress);
    sensorNameLabel.setText("Please enter a name for the OWL sensor with address "+sensorAddress);
    pack();
    UI.CenterInParent(electricOWLFrame, this);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    if( e.getSource() == okButton )
    {
      setVisible(false);
    }
    else if( e.getSource() == timer )
    {
      sensorDetectedTimeout--;
      if( sensorDetectedTimeout == 0 )
      {
        timer.stop();
        timer = null;
        setVisible(false);
        electricOWLFrame.printStatus("");
      }
      else
      {
        electricOWLFrame.printStatus(sensorNameField.getText()+ " will be used as the sensor name in "+sensorDetectedTimeout+" seconds.");
      }
    }
  }
   
  public void setVisible(boolean visible)
  {
    if( visible && timer == null )
    {
      sensorDetectedTimeout=20;
      timer = new Timer(1000, this);
      timer.setInitialDelay(1000);
      timer.start();         
    }
    pack();
    super.setVisible(visible);
  }
  
  /**
   * Get the Sensor name.
   * Either the default name of the name entered by the user.
   * This should not return an empty string.
   * 
   * @return
   */
  public String getSensorName()
  {
    String sensorName = sensorNameField.getText();
    if( sensorName.length() == 0 )
    {
      sensorName="MainHome";
      sensorNameField.setText(sensorName);
    }
    return sensorName;
  }
  
}
