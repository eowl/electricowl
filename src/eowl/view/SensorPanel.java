/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package eowl.view;

import javax.swing.*;

import eowl.model.ControllerConfig;
import eowl.model.SensorData;
import java.awt.*;
import lib.gui.*;
import java.awt.event.*;
import lib.shell.Executioner;

public class SensorPanel extends JPanel implements MouseListener
{
  private GridLayout 				gridLayout;
  private TimeSeriesPlotPanel 		ampsPlotPanel; 
  private TimeSeriesPlotPanel 		kwhPlotPanel; 
  private TimeSeriesPlotPanel 		costPlotPanel; 
  private boolean					aboveMaxOutputPower;
  private ElectricOWLFrame 			electricOWLFrame;
  
  public SensorPanel(ElectricOWLFrame electricOWLFrame)
  {
	this.electricOWLFrame=electricOWLFrame;
    gridLayout = new GridLayout();
    setLayout( gridLayout );
    addMouseListener(this);
  }
  
  /**
   * Determine how many rows of graphs we need
   * 
   * @param controllerConfig
   */
  private void setRowCount(ControllerConfig controllerConfig)
  {
    int rowCount=0;
    if( controllerConfig.viewAmps )
    {
      rowCount++;
    }
    if( controllerConfig.viewkWh )
    {
      rowCount++;
    }
    if( controllerConfig.viewCost )
    {
      rowCount++;
    }
    gridLayout.setRows(rowCount);
  }
  /**
   * Add a single reading to the plots.
   * 
   * @param sensorData
   */
  public void plotData(String sensorName, SensorData sensorData, ControllerConfig controllerConfig, long maxAgeSeconds, boolean allowExternalCommand)
  {
    setRowCount(controllerConfig);
    
    //If max trigger power is enabled and we are at or over the max power defined
    if( controllerConfig.maxPowerTriggerEnabled ) {
    	//If we were above the max output power and have now dropped below it then
    	if( aboveMaxOutputPower && sensorData.getKW(controllerConfig.voltage) < controllerConfig.maxPowerTriggerKWh ) {
    		//Reset flag
    		aboveMaxOutputPower=false;
    	}
    	//If above the max output power and previously the output power was below the maximum
    	else if( sensorData.getKW(controllerConfig.voltage) >= controllerConfig.maxPowerTriggerKWh && !aboveMaxOutputPower ) {
    		if( allowExternalCommand ) {
    			//Run external command
    			new Executioner(electricOWLFrame, sensorName, sensorData, controllerConfig).start();
    		}
	    	aboveMaxOutputPower=true;
	    }
    }
    else {
    	aboveMaxOutputPower=false;
    }

    if( controllerConfig.viewAmps )
    {
      if( ampsPlotPanel == null )
      {
        ampsPlotPanel = new TimeSeriesPlotPanel();
        ampsPlotPanel.maxAgeSeconds=maxAgeSeconds;
        ampsPlotPanel.xAxisName="Time";
        ampsPlotPanel.yAxisName="Amps";
        ampsPlotPanel.addPlot();
        add(ampsPlotPanel);
        validate();
      }
      ampsPlotPanel.addPlotValue(0, sensorData.getDate(), sensorData.getAmps(), aboveMaxOutputPower);
    }
    else
    {
      if( ampsPlotPanel != null )
      {
        ampsPlotPanel.removePlots();
        remove(ampsPlotPanel);
        ampsPlotPanel=null;
        validate();
      }      
    }
    
    if( controllerConfig.viewkWh )
    {
      if( kwhPlotPanel == null )
      {
        kwhPlotPanel = new TimeSeriesPlotPanel();
        kwhPlotPanel.maxAgeSeconds=maxAgeSeconds;
        kwhPlotPanel.xAxisName="Time";
        kwhPlotPanel.yAxisName="kW";
        kwhPlotPanel.addPlot();
        add(kwhPlotPanel);
        validate();
      }
      kwhPlotPanel.addPlotValue(0, sensorData.getDate(), sensorData.getKW(controllerConfig.voltage), aboveMaxOutputPower);
    }
    else
    {
      if( kwhPlotPanel != null )
      {
        kwhPlotPanel.removePlots();
        remove(kwhPlotPanel);
        kwhPlotPanel=null;
        validate();
      }      
    }
    
    if( controllerConfig.viewCost )
    { 
      if( costPlotPanel == null )
      {
        costPlotPanel = new TimeSeriesPlotPanel();
        costPlotPanel.maxAgeSeconds=maxAgeSeconds;
        costPlotPanel.xAxisName="Time";
        costPlotPanel.yAxisName="Cost("+controllerConfig.currencyString+")";
        costPlotPanel.addPlot();
        add(costPlotPanel);
        validate();
      }
      costPlotPanel.addPlotValue(0, sensorData.getDate(), sensorData.getCost(controllerConfig.voltage, controllerConfig.costPerkWh), aboveMaxOutputPower);
    }
    else
    {
      if( costPlotPanel != null )
      {
        costPlotPanel.removePlots();
        remove(costPlotPanel);
        costPlotPanel=null;
        validate();
      }      
    }
  }
  
  public void setNotify(boolean notify)
  {
    if( ampsPlotPanel != null )
    {
      ampsPlotPanel.setNotify(notify);
    }
    if( kwhPlotPanel != null )
    {
      kwhPlotPanel.setNotify(notify);
    }
    if( costPlotPanel != null )
    {
      costPlotPanel.setNotify(notify);
    }
  }
  
  public void mouseClicked(MouseEvent e) {}
  public void mouseEntered(MouseEvent e) {}
  public void mouseExited(MouseEvent e) {}
  public void mousePressed(MouseEvent e) {}
  public void mouseReleased(MouseEvent e) {}  

}
