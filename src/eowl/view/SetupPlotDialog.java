/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package eowl.view;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import lib.gui.*;
import java.text.*;

import eowl.model.*;

public class SetupPlotDialog extends JDialog implements ActionListener, KeyListener
{
  public static final String configFile = "eowl.view.SetupPlotDialogConfig.cfg";
  private SetupPlotDialogConfig setupPlotDialogConfig;
  private JPanel buttonPanel = new JPanel();
  private JButton okButton = new JButton("OK");
  private JButton cancelButton = new JButton("Cancel");
  private boolean okSelected;
  private DateTimePanel startDateTimePanel;
  private DateTimePanel stopDateTimePanel;
  private RowPane centrePanel;
  private JTextField logDaysCountField;
  private JLabel logDaysLabel;
  private JPanel logDaysPanel;
  private Frame frame;
  private JRadioButton secondsRadioButton  = new JRadioButton("Seconds");
  private JRadioButton minutesRadioButton  = new JRadioButton("Minutes");
  private JRadioButton hoursRadioButton  = new JRadioButton("Hours");
  private JRadioButton daysRadioButton  = new JRadioButton("Days");
  private JRadioButton weeksRadioButton  = new JRadioButton("Weeks");
  private ButtonGroup  timeButtonGroup = new ButtonGroup();
  private JPanel       plotResolutionPanel = new JPanel();
  private JComboBox    sensorComboBox = new JComboBox();
  private JPanel       sensorPanel = new JPanel();
  private Hashtable<String, Integer>sensorTable;
  
  public SetupPlotDialog(Frame frame)
  {
    super(frame, "History", true);
    this.frame=frame;
    
    setupPlotDialogConfig = new SetupPlotDialogConfig();
    try
    {
      setupPlotDialogConfig.load(configFile);
    }
    catch(Exception e) {}
    
    startDateTimePanel = new DateTimePanel("Start");
    stopDateTimePanel = new DateTimePanel("Stop");
    logDaysCountField = new JTextField(10);
    logDaysLabel = new JLabel("Days");
    logDaysPanel = new JPanel(new FlowLayout(FlowLayout.LEFT) );
    logDaysPanel.add(logDaysLabel);
    logDaysPanel.add(logDaysCountField);
    logDaysCountField.addKeyListener(this);
    
    centrePanel = new RowPane();
    centrePanel.add(startDateTimePanel);
    centrePanel.add(logDaysPanel);
    centrePanel.add(stopDateTimePanel);
    
    timeButtonGroup.add(secondsRadioButton);
    timeButtonGroup.add(minutesRadioButton);
    timeButtonGroup.add(hoursRadioButton);
    timeButtonGroup.add(daysRadioButton);
    timeButtonGroup.add(weeksRadioButton);
    
    plotResolutionPanel.add(secondsRadioButton);
    plotResolutionPanel.add(minutesRadioButton);
    plotResolutionPanel.add(hoursRadioButton);
    plotResolutionPanel.add(daysRadioButton);
    plotResolutionPanel.add(weeksRadioButton);
    plotResolutionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY) ,"Resolution", TitledBorder.LEFT, TitledBorder.BELOW_TOP));
    
    centrePanel.add(plotResolutionPanel);
    sensorPanel.add(new JLabel("Sensor"));
    sensorPanel.add(sensorComboBox);
    centrePanel.add(sensorPanel);
    
    getContentPane().add(centrePanel, BorderLayout.CENTER);
        
    okButton.addActionListener(this);
    cancelButton.addActionListener(this);
    buttonPanel.add(okButton);
    buttonPanel.add(cancelButton);
    getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    pack();
    
    logDaysCountField.setText("");
    startDateTimePanel.addActionListener(this);
    stopDateTimePanel.addActionListener(this);
    if( setupPlotDialogConfig.xPos > 0 && setupPlotDialogConfig.yPos > 0 )
    {
      //Set the last location for the dialog
      setLocation(setupPlotDialogConfig.xPos, setupPlotDialogConfig.yPos);      
    }
    else
    {
      UI.CenterOnScreen(this);
    }
    
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    Calendar cal=Calendar.getInstance();
    Date date;
    String dateString;
    try
    {
      dateString = ""+setupPlotDialogConfig.startYear+"-"+setupPlotDialogConfig.startMonth+"-"+setupPlotDialogConfig.startDay+" "+setupPlotDialogConfig.startHour+":"+setupPlotDialogConfig.startMin;
      date = df.parse( dateString );
      cal.setTime(date);
      startDateTimePanel.setCalendar(cal);
      
      dateString = ""+setupPlotDialogConfig.stopYear+"-"+setupPlotDialogConfig.stopMonth+"-"+setupPlotDialogConfig.stopDay+" "+setupPlotDialogConfig.stopHour+":"+setupPlotDialogConfig.stopMin;
      date = df.parse( dateString );
      cal.setTime(date);
      stopDateTimePanel.setCalendar(cal);
    }
    catch(ParseException e) 
    {
      e.printStackTrace();
    }
  }
  
  private void saveConfig()
  {
    Calendar startCalendar = startDateTimePanel.getCalendar();
    setupPlotDialogConfig.startMin=startCalendar.get(Calendar.MINUTE);
    setupPlotDialogConfig.startHour=startCalendar.get(Calendar.HOUR_OF_DAY);
    setupPlotDialogConfig.startDay=startCalendar.get(Calendar.DAY_OF_MONTH);
    //Save month in config as 1-12
    setupPlotDialogConfig.startMonth=startCalendar.get(Calendar.MONTH)+1;
    setupPlotDialogConfig.startYear=startCalendar.get(Calendar.YEAR);
    
    Calendar stopCalendar = stopDateTimePanel.getCalendar();
    setupPlotDialogConfig.stopMin=stopCalendar.get(Calendar.MINUTE);
    setupPlotDialogConfig.stopHour=stopCalendar.get(Calendar.HOUR_OF_DAY);
    setupPlotDialogConfig.stopDay=stopCalendar.get(Calendar.DAY_OF_MONTH);
    //Save month in config as 1-12
    setupPlotDialogConfig.stopMonth=stopCalendar.get(Calendar.MONTH)+1;
    setupPlotDialogConfig.stopYear=stopCalendar.get(Calendar.YEAR);

    if( secondsRadioButton.isSelected() )
    {
      setupPlotDialogConfig.plotPeriodMS = 1000;
    }
    else if( minutesRadioButton.isSelected() )
    {
      setupPlotDialogConfig.plotPeriodMS = 1000*60;
    }  
    else if( hoursRadioButton.isSelected() )
    {
      setupPlotDialogConfig.plotPeriodMS = 1000*60*60;
    }  
    else if( daysRadioButton.isSelected() )
    {
      setupPlotDialogConfig.plotPeriodMS = 1000*60*60*24;
    }  
    else if( weeksRadioButton.isSelected() )
    {
      setupPlotDialogConfig.plotPeriodMS = 1000*60*60*24*7;
    }  
    else
    {
      setupPlotDialogConfig.plotPeriodMS = 1000*60;
    }
    
    setupPlotDialogConfig.selectedSensorIndex = sensorComboBox.getSelectedIndex();
    
    Point p = getLocation();
    setupPlotDialogConfig.xPos=p.x;
    setupPlotDialogConfig.yPos=p.y;
    try
    {
      setupPlotDialogConfig.save(configFile);
    }
    catch(Exception e) {}
  }
  
  public void setVisible(boolean visible)
  {
    okSelected=false;
    update();
    super.setVisible(visible);
  }
  
  public Date getStartDate()
  {
    return startDateTimePanel.getCalendar().getTime();
  }
  
  public Date getStopDate()
  {
    return stopDateTimePanel.getCalendar().getTime();
  }
   
  public void actionPerformed(ActionEvent e)
  {
    if( e.getSource() == okButton )
    {
      if( getDays() < 0 )
      {
        Dialogs.showErrorDialog(frame, "Error", "The start date occurs after the stop date");
        return;
      }
      saveConfig();
      setVisible(false);
      okSelected=true;
    }
    if( e.getSource() == cancelButton )
    {
      setVisible(false);
      okSelected=false;
    }
    if( e.getSource() == startDateTimePanel )
    {
      update();
    }
    if( e.getSource() == stopDateTimePanel )
    {
      update();
    }
  }
  
  /**
   * Return the number of days between the start and stop dates
   * 
   * @return days
   */
  private int getDays()
  {
    long diff = stopDateTimePanel.getCalendar().getTimeInMillis() - startDateTimePanel.getCalendar().getTimeInMillis();
    return (int)(((diff/1000)/60)/60)/24;
  }
  
  /**
   * Get the number of milli seconds between each plot point.
   * 
   * @return plot period in milli seconds
   */
  public int getPlotPeriodMS()
  {
    return setupPlotDialogConfig.plotPeriodMS;
  }
  
  public void update()
  {
    long diff = stopDateTimePanel.getCalendar().getTimeInMillis() - startDateTimePanel.getCalendar().getTimeInMillis();
    //If stop date is earlier than start date
    if( diff < 0 )
    {
      //set them both the same
      stopDateTimePanel.setCalendar(startDateTimePanel.getCalendar());
      logDaysCountField.setText("0");
    }    
    int days = getDays();
    //Set how many days 
    logDaysCountField.setText(""+days);
    
    if( setupPlotDialogConfig.plotPeriodMS == 1000 )
    {
      secondsRadioButton.setSelected(true);
    }
    else if( setupPlotDialogConfig.plotPeriodMS == 1000*60 )
    {
      minutesRadioButton.setSelected(true);
    }
    else if( setupPlotDialogConfig.plotPeriodMS == 1000*60*60 )
    {
      hoursRadioButton.setSelected(true);
    }
    else if( setupPlotDialogConfig.plotPeriodMS == 1000*60*60*24 )
    {
      daysRadioButton.setSelected(true);
    }
    else if( setupPlotDialogConfig.plotPeriodMS == 1000*60*60*24*7 )
    {
      weeksRadioButton.setSelected(true);
    }
    //Default to minutes, if value invalid
    else
    {
      minutesRadioButton.setSelected(true);
    }
  }
  
  /**
   * Return true if ok was selected
   * 
   * @return
   */
  public boolean wasOKSelected()
  {
    return okSelected;
  }
  
  /**
   * Set the sensors that are available
   * 
   * @param sensorTable
   */
  public void setAvailableSensors(Hashtable<String, Integer>sensorTable)
  {
	this.sensorTable=sensorTable;
    sensorComboBox.removeAllItems();
    int itemCount=0;
    for( String sensorName : sensorTable.keySet() )
    {
      sensorComboBox.addItem(sensorName);
      itemCount++;
    }
    if( setupPlotDialogConfig.selectedSensorIndex != -1 && setupPlotDialogConfig.selectedSensorIndex < itemCount )
    {
      sensorComboBox.setSelectedIndex(setupPlotDialogConfig.selectedSensorIndex);
    }
    else if( itemCount > 0 )
    {
      sensorComboBox.setSelectedIndex(0);
    }
  }
  
  /**
   * Get the sensor address
   * 
   * @return The address of the selected sensor
   */
  public int getSelectedSensorAddress()
  {
	  int index = sensorComboBox.getSelectedIndex();
	  //If no sensor was selected select the first one
	  if( index == -1 )
	  {
		  sensorComboBox.setSelectedIndex(0);
	  }
	  String sensorName = sensorComboBox.getSelectedItem().toString();
	  return sensorTable.get(sensorName);
  }
  
  /**
   * Get the selected sensor name
   * 
   * @return
   */
  public String getSelectedSensorName()
  {
    return sensorComboBox.getSelectedItem().toString();
  }
  
  public void keyPressed(KeyEvent e) {}
  public void keyReleased(KeyEvent e)
  {
    Calendar today = Calendar.getInstance();
    Calendar calendar = startDateTimePanel.getCalendar();
    int days;
    try
    {
      days = Integer.parseInt( logDaysCountField.getText() );
      calendar.add(Calendar.DATE, days);
      //If the date/time is later than now
      if( calendar.compareTo(today) > 0 )
      {
        calendar = today;
      }
      //Set the stop tim to this date/time
      stopDateTimePanel.setCalendar(calendar);
    }
    catch(NumberFormatException ex) 
    {
      logDaysCountField.setText("");
    }
  }
  public void keyTyped(KeyEvent e) {}
}
