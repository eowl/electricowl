/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package eowl.view;

import javax.swing.*;

import eowl.model.GUIConfig;
import eowl.model.SensorData;
import eowl.model.ControllerConfig;

import java.awt.event.*;
import java.awt.*;
import eowl.controller.*;
import lib.gui.*;

import java.util.*;
import java.text.DecimalFormat;

public class ElectricOWLFrame extends JFrame implements WindowListener
{
  public static final String 		GUI_CONFIG_FILE = "eowl.model.GUIConfig";
  private GUIConfig 				guiConfig;
  private ElectricOWLController 	electricOWLController;
  private StatusBar 				statusBar;
  private JTabbedPane 				tabbedPane = new JTabbedPane();
  private Vector<SensorPanel>sensorPanelList;
  private boolean 					firstSensorReading=true;
  private JMenuBar 					menuBar;
  private JMenu 					fileMenu;
  private JMenuItem 				setupMenuItem, exitMenuItem, openFileMenuItem , clearLogMenuItem, initUSBConnect, initSensorList;
  private DecimalFormat 			genericLogFormat = new DecimalFormat("###.##");
  private DecimalFormat 			costFormat = new DecimalFormat("###.#####");

  public ElectricOWLFrame(String title, ElectricOWLController electricOWLController)
  {
    super(title);
    this.electricOWLController=electricOWLController;
    guiConfig = new GUIConfig();
    try
    {
      guiConfig.load(GUI_CONFIG_FILE);
    }
    catch(Exception e) {}
    
    setSize(guiConfig.fWidth, guiConfig.fHeight);
    if( guiConfig.xPos == -1 && guiConfig.yPos == -1 )
    {
      UI.CenterOnScreen(this);
    }
    else
    {
      setLocation(guiConfig.xPos, guiConfig.yPos);
    }
    setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    
    initSensors();
    
    statusBar = new StatusBar(true);
    getContentPane().add(tabbedPane, BorderLayout.CENTER);
    getContentPane().add(statusBar, BorderLayout.SOUTH);
    menuBar = new JMenuBar();
    fileMenu = new JMenu("File");
    fileMenu.setMnemonic(KeyEvent.VK_F);
    menuBar.add(fileMenu);

    initUSBConnect = new JMenuItem("Init USB Connect", KeyEvent.VK_I);
    initUSBConnect.setAccelerator(KeyStroke.getKeyStroke( KeyEvent.VK_I, ActionEvent.ALT_MASK));
    initUSBConnect.getAccessibleContext().setAccessibleDescription( "Init the USB Connect device");
    fileMenu.add(initUSBConnect);
    initUSBConnect.addActionListener(electricOWLController);
    
    initSensorList = new JMenuItem("Init Sensor List", KeyEvent.VK_T);
    initSensorList.setAccelerator(KeyStroke.getKeyStroke( KeyEvent.VK_T, ActionEvent.ALT_MASK));
    initSensorList.getAccessibleContext().setAccessibleDescription( "Init Sensor List");
    fileMenu.add(initSensorList);
    initSensorList.addActionListener(electricOWLController);
    
    openFileMenuItem = new JMenuItem("Open History", KeyEvent.VK_S);
    openFileMenuItem.setAccelerator(KeyStroke.getKeyStroke( KeyEvent.VK_F, ActionEvent.ALT_MASK));
    openFileMenuItem.getAccessibleContext().setAccessibleDescription( "Open and display logfile contents");
    fileMenu.add(openFileMenuItem);
    openFileMenuItem.addActionListener(electricOWLController);
 
    clearLogMenuItem = new JMenuItem("Clear Log", KeyEvent.VK_L);
    clearLogMenuItem.setAccelerator(KeyStroke.getKeyStroke( KeyEvent.VK_L, ActionEvent.ALT_MASK));
    clearLogMenuItem.getAccessibleContext().setAccessibleDescription( "Clear the log file");
    fileMenu.add(clearLogMenuItem);
    clearLogMenuItem.addActionListener(electricOWLController);

    setupMenuItem = new JMenuItem("Setup", KeyEvent.VK_S);
    setupMenuItem.setAccelerator(KeyStroke.getKeyStroke( KeyEvent.VK_S, ActionEvent.ALT_MASK));
    setupMenuItem.getAccessibleContext().setAccessibleDescription( "Change the Electric OWL setup");
    fileMenu.add(setupMenuItem);
    setupMenuItem.addActionListener(electricOWLController);
    
    exitMenuItem = new JMenuItem("Exit", KeyEvent.VK_E);
    exitMenuItem.setAccelerator(KeyStroke.getKeyStroke( KeyEvent.VK_E, ActionEvent.ALT_MASK));
    exitMenuItem.getAccessibleContext().setAccessibleDescription( "Kill the Electric OWL");
    fileMenu.add(exitMenuItem);
    exitMenuItem.addActionListener(electricOWLController);
    
    setJMenuBar(menuBar);

    addWindowListener(this);
  }
  
  /**
   * This line will be displayed in the status bar and will disappear after 3 seconds or so.
   * @param line
   */
  public void printStatus(String line)
  {
    statusBar.println(line);
  }
  
  /**
   * This message will be printed to the status bar and will stay displayed.
   * @param line
   */
  public void printStatusPersist(String line)
  {
    statusBar.println_persistent(line);
  }
   
  public void windowActivated(WindowEvent e) {}
  public void windowClosed(WindowEvent e) {}
  public void windowClosing(WindowEvent e) 
  {
    guiConfig.fWidth=getWidth();
    guiConfig.fHeight=getHeight();
    Point p = getLocation();
    guiConfig.xPos = p.x;
    guiConfig.yPos = p.y;
    try
    {
      guiConfig.save(GUI_CONFIG_FILE);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
    electricOWLController.shutdown();
  }
  public void windowDeactivated(WindowEvent e) {}
  public void windowDeiconified(WindowEvent e) {}
  public void windowIconified(WindowEvent e) {}
  public void windowOpened(WindowEvent e) {}

  public void initSensors()
  {
    sensorPanelList = new Vector<SensorPanel>();
    tabbedPane.removeAll();
  }
  
  /**
   * Add a single reading to the plots.
   * 
   * @param sensorData
   */
  public void plotData(String sensorName, SensorData sensorData, ControllerConfig controllerConfig)
  {
    SensorPanel sensorPanel;
    int tabIndex=-1;
    
    //Check through all the tabs to see if a tab already exists for this sensor
    for( int i=0 ; i<tabbedPane.getTabCount() ; i++ )
    {
      if( tabbedPane.getTitleAt(i).equals(sensorName) )
      {
        tabIndex=i;
        break;
      }
    }
    //If we don't have a tab for this sensor
    if( tabIndex == -1 )
    {
      //Create one
      tabIndex=tabbedPane.getTabCount();
      sensorPanel = new SensorPanel(this);
      sensorPanelList.add(sensorPanel);
      JScrollPane scrollPane = new JScrollPane(sensorPanel);
      tabbedPane.addTab(sensorName, scrollPane);
    }
    //Add line of text to status bar log (not to status bar in frame)
    if( firstSensorReading )
    {
      statusBar.log("Sensor Name,Amps,KW,Cost"); 
      firstSensorReading=false;
    }
    
    //Format the number for display in the log
    statusBar.println(sensorName+","+genericLogFormat.format(sensorData.getAmps())+","+genericLogFormat.format(sensorData.getKW(controllerConfig.voltage))+","+costFormat.format(sensorData.getCost(controllerConfig.voltage, controllerConfig.costPerkWh))); 
    sensorPanelList.get(tabIndex).plotData(sensorName, sensorData, controllerConfig, controllerConfig.maxPlotAgeSecs, true);
    sensorPanelList.get(tabIndex).setNotify(true);
  }  
}
