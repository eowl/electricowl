/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package eowl.controller;

import java.net.ServerSocket;
import java.io.*;

import eowl.view.*;
import eowl.model.*;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.*;

import lib.gui.Dialogs;
import lib.gui.UI;
import lib.io.FileIO;
import lib.shell.Executioner;

import java.awt.event.*;




/**
 * Change log
 * 
 * 
 * V1.8
 * - Problem introduced in V1.7. OWLServer.exe, .net target changed to 3.5 due to corruption of the SharpDevelop project.
 *   Rebuilt SharpDevelop OWLServer project (set target as 32 bit, .net 2.0) and recompiled.
 * - Take more care of the error handling when converting old log file formats to new log file formats in the 
 *   OWLServer code to handle situations where the log file does not exist and where the log file is empty.
 * - On the Electric OWL client side if Double values are passed with comma's in convert them to . separated
 *   work around localisation differences between dotnet and Java.
 * 
 * V1.7
 * - Change the format of OWLServer log files to be semicolon separated rather than 
 *   comma separated. This is because in France localisation may cause number to 
 *   have comma's in them, E.G
 *   
 *   SENSOR_DATA=383,2,59,19,79248740,27,2010-06-19 07:01:41
 *   instead of this
 *   SENSOR_DATA=383,2.59,19,79248740.27,2010-06-19 07:01:41
 *   
 *   This change involved 
 *   - Changing the format the file was saved (use semicolons to separated the data rather than commas) by the OWLServer.
 *   - Adding code to convert existing log files from comma separated to semicolon separated text when the OWLServer starts up.
 *   - Changing the Java client code to expect semicolon separated data rather than comma separated data from the OWLServer.
 *   
 *   Therefore the first time the new OWLServer starts up it will convert the log file (C:\owl_server_log.txt) from the 
 *   old format to the new format. The old file will be renamed to C:\owl_server_log.txt.original in this process.
 *   The time taken to perform this conversion will vary based on the spec of the machine that it's running on but 
 *   should take around 20 seconds pre 100 MB of log file.
 *   
 *   Thanks to kalemena for testing this.
 *   
 * - Getting historical data could be error prone when large log files are present.
 *   The OWLServer and Electric OWL code to get historical data has been restructured
 *   and the design changed in order to make the progress bar update more 
 *   accurate (the progress bar now represents how far the search through 
 *   the log data has progressed). Also the cancel button on the progress 
 *   bar will stop the log being sent to the client (Electric OWL) from 
 *   the server (OWLServer) as soon as the cancel button is selected.
 *   Previously the user had to wait for the entire log to be sent 
 *   before the OWLServer would continue to log data.
 * 
 * V1.6
 * - Bump to next general release version number.
 * 
 * V1.54
 * - Add a feature where the user can set a max power limit (in KW) in the 
 *   configuration dialog. When the power detected is greater than this
 *   value and external command (user configurable) can be executed. 
 *   Also when the power is over the configured maximum they are highlighted
 *   on the plots in red to indicate the period of time the power usage was
 *   over the configured maximum. 
 * - Add tool tips for each setup/config dialog menu item.
 * - Allow the user to adjust the max plot time for the main plot up to a
 *   max of 1 hour. 
 * - Modify windows installer to provide the option of running the OWLServer 
 *   on startup.
 * - Fix problem with installer: When requested to install a desktop icon, no
 *   desktop icon was installed.
 * - Updated the manual document to include the changes to the GUI.
 * 
 * V1.53
 * - Removed init sensors option from config dialog and added to the File menu.
 * 
 * V1.52
 * - Recompiled the OWLServer so that it runs on 64 Bit platforms.
 * 
 * V1.51
 * - Added an init option to the main menu to init the USB connect device attached 
 *    to the server in order to see if this will recover the situation when the 
 *    USB connect device appears to lock up and stop detecting sensors.
 * 
 * V1.5
 * - Change Y axis label on cost graph to Cost(�) rather than 'Cost (�) per kWh'
 * - When GUI made visible of hidden the plot updates can be delayed as the CPU 
 *   spends it's time displaying the window. If this delay is long enough to span 
 *   the gap between polls then an exception is thrown because the plot occur 
 *   in the same time period. Fixed by using addOrUpdate rather than add which 
 *   will not throw an exception in the event of a overlapping time period.
 * - Provide definition of the max digit count of numbers shown in the log file 
 *   to limit them to sensible resolutions. 
 * - When starting up, the status log now details the location of the OWLServer 
 *   logfile.
 * - Added an exception stack trace to the status log to aid debugging of any 
 *   problems reported.
 * - When a sensor is detected present a default name for the sensor (Mains 
 *   followed by the sensor address) and auto select this sensor name if the 
 *   user does not enter a different name within 20 seconds.
 *   Previously if a sensor was found and the user did not enter a name when 
 *   the dialog was presented it would block the update of other sensors which
 *   caused the stream buffers on the server connection to fill up.
 *   
 * V1.4
 * The history plot is now in a useful state.
 *
 * V1.3
 * Fixed misc bugs in the V1.2.
 *
 * v1.2
 * Change so that OwlServer logs data and GUI retrives data from history held at Owl server
 * 
 * v1.11
 * Started log file impl
 * 
 * v1.1
 * Fix negative period value when Electric OWL just installed
 * Set default cost to 0.01 
 * 
 * * v1.0
 * Initial release.
 * 
 */
public class ElectricOWLController implements ActionListener
{
  public static double          VERSION=1.8;
  public static final String    CONTROLLER_CONFIG_FILE = "eowl.model.ControllerConfig";
  public static final String 	SENSOR_LIST_FILE = "eowl.sensor_list.txt";
  private ControllerConfig      controllerConfig;
  private ElectricOWLFrame      electricOWLFrame;
  private ElectricOWLServerInterface     electricOWLPoller;
  private Process               owlServerProcess;
  private boolean               sensorDetected=false;
  private SetupDialog           setupDialog;
  private SetupPlotDialog       setupPlotDialog;
  private InputSensorNameDialog inputSensorNameDialog;
  private static File 			SensorListFile;
  
  public ElectricOWLController()
  {
    controllerConfig = new ControllerConfig();
    try
    {
      controllerConfig.load(CONTROLLER_CONFIG_FILE);
    }
    catch(Exception e) 
    {
      //e.printStackTrace();
    }
    
    try
    {
      UIManager.LookAndFeelInfo[] lookAndFeels = UIManager.getInstalledLookAndFeels();
      UIManager.setLookAndFeel(lookAndFeels[controllerConfig.lookAndFeelIndex].getClassName());
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    
    electricOWLFrame = new ElectricOWLFrame("Electric OWL (V"+VERSION+")", this);
    if( controllerConfig.runLocalOWLServer )
    {
      try
      {
        controllerConfig.serverPort = getFreeTCPPort();
        info("Starting OWL server on TCP/IP port "+controllerConfig.serverPort);
        owlServerProcess = Runtime.getRuntime().exec("OwlServer.exe -q -p "+controllerConfig.serverPort);
        //Allow the server time to startup
        Thread.sleep(500);
      }
      catch(Exception e)
      {
        error(e);
      }
    }
    setupDialog = new SetupDialog(electricOWLFrame);
    UI.CenterOnScreen(setupDialog);
    electricOWLPoller = new ElectricOWLServerInterface(this, controllerConfig);
    electricOWLPoller.start();
    
    setupPlotDialog = new SetupPlotDialog(electricOWLFrame);
    
    inputSensorNameDialog = new InputSensorNameDialog(electricOWLFrame);
    
	String userhome = System.getProperty("user.home");
	SensorListFile = new File(userhome, SENSOR_LIST_FILE);
  }
  
  /**
   * Display an error message in the electricOWLFrames status bar
   * 
   * @param line
   */
  public void error(Exception e)
  {
    //Add debug info
    electricOWLFrame.printStatus("Developer debugging information");
    electricOWLFrame.printStatus("Exception type: "+e.getClass());
    for (StackTraceElement element : e.getStackTrace() ){
      electricOWLFrame.printStatus(""+element);
    }

    //Display the exception text so that it is easily read by the user
    String message = e.getLocalizedMessage();
    if ( message != null && message.length() > 0 )
    {
      electricOWLFrame.printStatusPersist("ERROR: "+message);
    }
    else
    {
      electricOWLFrame.printStatusPersist("UNKNOWN ERROR");
    }
  }
  
  /**
   * Display an information message in the electricOWLFrames status bar
   * 
   * @param line
   */
  public void info(String line)
  {
    electricOWLFrame.printStatusPersist(line);
  }

  /**
   * Get a free local TCP/IP port.
   * 
   * @return The TCP IP port.
   */
  public int getFreeTCPPort() throws IOException
  { 
    ServerSocket serverSocket = new ServerSocket();
    serverSocket.bind(null);
    if( serverSocket == null )
    {
      return -1;
    }
    int freePort = serverSocket.getLocalPort();
    serverSocket.close();
    serverSocket=null;
    return freePort; 
  }

  /**
   * Start the controller
   */
  public void start()
  {
    electricOWLFrame.setVisible(true);
  }
  
  /**
   * Called when the electricOWLFrame closes
   */
  public void shutdown()
  {
    if ( owlServerProcess != null )
    {
      owlServerProcess.destroy();
    }
    owlServerProcess=null;
  }
  
  /**
   * Called by the ElectricOWLPoller after sensor data has been read from the OWL USB Connect device.
   * If the list has zero elements then this indicates that the USB connect device is not receiving data from any sensors.
   * This method is also responsible for firing an external command when the power is over the max trigger power, if 
   * enable max trigger power is selected in the config dialog.
   * 
   * @param sensorDataList
   */
  public void record(Vector<SensorData>sensorDataList)
  {
    String sensorName;
    if( sensorDataList.size() == 0 )
    {
      info("Checking for sensors, this may take a minute or two, please wait...");
    }
    else
    {
      //The first time we detect a sensor display a status message
      if( !sensorDetected )
      {
        if( sensorDataList.size() == 1 )
        {
          info("Recording data from 1 sensor");    
        }
        else
        {
          info("Recording data from "+sensorDataList.size()+" sensors");      
        }        
      }
      sensorDetected=true;
    }
    for( SensorData sensorData : sensorDataList )
    {
      saveToLogFile(sensorData, controllerConfig.logFile );
      //Get the stored sensor name
      sensorName = controllerConfig.getSensorName(sensorData.getAddress());
      //If the user has not entered a name for this sensor yet
      if( sensorName == null )
      {
        sensorName = getSensorNameInteractive(sensorData.getAddress());
        if( controllerConfig.sensorAddrNames.length() == 0 )
        {
          //Start a list of sensor names
          controllerConfig.sensorAddrNames=sensorData.getAddress()+"="+sensorName;
        }
        else
        {   
          //Add to the sensor names
          controllerConfig.sensorAddrNames=controllerConfig.sensorAddrNames+","+sensorData.getAddress()+"="+sensorName;
        }
        updateSensorList(sensorName, sensorData);
        ElectricOWLController.SaveControllerConfig(controllerConfig);
      }
      electricOWLFrame.plotData(sensorName, sensorData, controllerConfig);
    }
  }
  
  /**
   * UPdate the sensor list file.
   */
  private void updateSensorList(String sensorName, SensorData sensorData)
  {
	  Properties sensorProperties = new Properties();
	  boolean addressFound=false;
	  try
	  {
		  sensorProperties = FileIO.GetProperties(SensorListFile);	
		  Enumeration enumeration = sensorProperties.propertyNames();
		  while( enumeration.hasMoreElements() ) 
		  {
			  String key = enumeration.nextElement().toString();
			  int sensorAddressFromFile = Integer.parseInt(key);
			  if( sensorData.getAddress() == sensorAddressFromFile )
			  {
				  addressFound=true;
			  }
		  }
		  //If the sensor address is currently in the properties file
		  if( addressFound ) {
			  //Remove this sensor address
			  sensorProperties.remove(""+sensorData.getAddress());
			  //Add the sensor address/name
			  sensorProperties.put(""+sensorData.getAddress(), sensorName);
			  electricOWLFrame.printStatus("Replaced "+sensorName+" in the sensor list");
		  }
		  //Add the sensor details to the list
		  else {
			  //Add the sensor address/name
			  sensorProperties.put(""+sensorData.getAddress(), sensorName);			  
			  electricOWLFrame.printStatus("Added "+sensorName+" to the sensor list");
		  }
	  }
	  catch(IOException e) {}
	  {
		  //electricOWLFrame.printStatus(e.getLocalizedMessage());
	  }
	  try
	  {
		  //Save the sensor details to a file
		  FileIO.SetProperties(sensorProperties, SensorListFile, "ElectricOWL Sensor Properties");
	  }
	  catch(IOException e)
	  {
		  e.printStackTrace();
	  }
  }
  
  
  /**
   * Save data to log file
   */
  public void saveToLogFile(SensorData sensorData, File logFile)
  {
    //If we have a log file configured
    if( logFile.getName().length() > 0 )
    {
      PrintWriter pw = null;
      try
      {
        pw = new PrintWriter( new FileWriter(logFile.getAbsolutePath(), true) );
        pw.println(sensorData.getAddress()+","+sensorData.getAmps()+","+sensorData.getModel()+","+sensorData.getAccumulatedAmps()+","+sensorData.getDate());
      }
      catch(Exception e)
      {
        error(e);
      }
      finally
      {
        if( pw != null )
        {
          pw.close();
        }
      }
    }
  }
  
  
  /**
   * Save the controller config persistently
   */
  public static void SaveControllerConfig(ControllerConfig controllerConfig)
  {
    try
    {
      controllerConfig.save(CONTROLLER_CONFIG_FILE);
    }
    catch(Exception e)
    {
//      e.printStackTrace();
    }
  }
  
  
  /**
   * Allow user to enter a name for the given sensor.
   * 
   * @param sensorAddress
   * @return
   */
  public String getSensorNameInteractive(int sensorAddress)
  {
    String sensorName=null;
    inputSensorNameDialog.setSensorAddress(sensorAddress);
    inputSensorNameDialog.setVisible(true);
    sensorName = inputSensorNameDialog.getSensorName();
    return sensorName;
  }
  
  public void actionPerformed(ActionEvent e)
  {
    if( e.getActionCommand().equals("Exit") )
    {
      shutdown();
      System.exit(0);
    }
    //Setup Electric OWL
    if( e.getActionCommand().equals("Setup") )
    {
      setupDialog.setControllerConfig(controllerConfig);
      setupDialog.setVisible(true);
      if( setupDialog.OKSelected() )
      {
        int initialSensorNamesLength=controllerConfig.sensorAddrNames.length();
        controllerConfig = setupDialog.getControllerConfig(controllerConfig);
        //If the user has elected to setup the sensors
        if( initialSensorNamesLength > 0 && controllerConfig.sensorAddrNames.length() == 0 )
        {
          sensorDetected=false;
          electricOWLFrame.initSensors();
        }
        ElectricOWLController.SaveControllerConfig(controllerConfig);
      }
    }
    //Open log file command
    if( e.getActionCommand().equals("Open History") )
    {
      setupPlotDialog.setAvailableSensors( GetSensorHashTable() );
      
      setupPlotDialog.setVisible(true);
      if( setupPlotDialog.wasOKSelected() )
      {
        class PlotThread extends Thread
        {
          public void run()
          {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm yyyy-MMM-dd");
            Date startDate = setupPlotDialog.getStartDate();
            Date stopDate = setupPlotDialog.getStopDate();
            String startDateString = simpleDateFormat.format( startDate );            
            String stopDateString = simpleDateFormat.format( stopDate );
            String title = "History ("+startDateString+ " until " + stopDateString+")";
            PlotFrame plotFrame = new PlotFrame(electricOWLFrame, controllerConfig, title, electricOWLFrame.getWidth(), electricOWLFrame.getHeight() );
            int sensorAddress = setupPlotDialog.getSelectedSensorAddress();
            electricOWLPoller.plot(setupPlotDialog.getStartDate(), setupPlotDialog.getStopDate(),  setupPlotDialog.getPlotPeriodMS(), sensorAddress, plotFrame);
          }
        }
        new PlotThread().start();
      }
    }
    //Open log file command
    if( e.getActionCommand().equals("Clear Log") )
    {
      int response = Dialogs.showYesNoDialog(electricOWLFrame, "Clear log", "Are you sure that you wish to clear the log ?");
      if( response == JOptionPane.YES_OPTION )
      {
        electricOWLPoller.clearLog();
      }
    }
    
    //If required to init the USB connect device
    if( e.getActionCommand().equals("Init USB Connect") )
    {
      class Executioner extends Thread 
      {
        public void run() {
          if( electricOWLPoller != null ) {
            try {
              electricOWLPoller.initUSBConnect();
              JOptionPane.showMessageDialog(electricOWLFrame, "USB connect device successfully initialised.");
            }
            catch(Exception e) 
            {
              JOptionPane.showMessageDialog(electricOWLFrame, "Unable to initialise the USB connect device.", "Error", JOptionPane.ERROR_MESSAGE);
              error(e);
            }
          }
        }
      }
      new Executioner().start();   
    }
    
    //Open log file command
    if( e.getActionCommand().equals("Init Sensor List") )
    {
    	controllerConfig.sensorAddrNames="";
        sensorDetected=false;
        electricOWLFrame.initSensors();
    }

  }
  
  public static Hashtable<String, Integer> GetSensorHashTable()
  {
	  Hashtable<String, Integer>sensorTable = new Hashtable<String, Integer>();
	  
	  try
	  {
		  Properties sensorProperties = FileIO.GetProperties(SensorListFile);
		  Enumeration enumeration = sensorProperties.propertyNames();
		  while( enumeration.hasMoreElements() ) 
		  {
			  String sensorAddressString = enumeration.nextElement().toString();
			  int sensorAddress = Integer.parseInt(sensorAddressString);
			  String sensorName = sensorProperties.getProperty(""+sensorAddress);
			  sensorTable.put(sensorName, sensorAddress);
		  }	  
	  }
	  catch(IOException e) {}
	  return sensorTable;
  }
  
}
