/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package eowl.controller;

import java.net.Socket;
import java.util.*;
import eowl.model.*;
import eowl.view.PlotFrame;
import java.text.SimpleDateFormat;

import javax.swing.ProgressMonitor;
import java.io.*;

import lib.gui.Dialogs;

public class ElectricOWLServerInterface extends Thread
{
  private ElectricOWLController electricOWLController;
  private ControllerConfig controllerConfig;
  private Socket socket;
  private BufferedWriter bw;
  private BufferedReader br;
  public static final int MAX_HISTORY_PLOT_POINT_COUNT=40000;

  public ElectricOWLServerInterface(ElectricOWLController electricOWLController, ControllerConfig controllerConfig)
  {
    this.electricOWLController=electricOWLController;
    this.controllerConfig=controllerConfig;
  }
  
  public void run()
  {
    SensorData sensorData;
    Vector<SensorData> sensorDataList = new Vector<SensorData>();
    String line;
    bw=null;
    br=null;
    
    try
    {
      electricOWLController.info("Connecting to OWL server ("+controllerConfig.serverAddress+":"+controllerConfig.serverPort+")");

      socket = new Socket(controllerConfig.serverAddress, controllerConfig.serverPort);
      br = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );
      bw = new BufferedWriter( new OutputStreamWriter( socket.getOutputStream() ) );
      
      electricOWLController.info("Connected to OWL server");

      electricOWLController.info("Logfile: C:\\owl_server_log.txt (on machine running OWLServer)");
      electricOWLController.info("Current log file size is "+getLogSize()/1E6+" MB");
      
      while(true)
      {
        //Ensure this block executes in it's entirety
        synchronized(this)
        {
          bw.write("GET\n");
          bw.flush();
          while(true)
          {
            line=br.readLine();
            if( line == null )
            {
              break;
            }
            //Useful for debugging
            //electricOWLController.info(line);
            if( line.indexOf("GET_COMPLETE") != -1 )
            {
              electricOWLController.record(sensorDataList);
              sensorDataList = new Vector<SensorData>();
              break;
            }
            //If we have some sensor data
            sensorData = new SensorData(line);
            sensorDataList.add( sensorData );
          }
        }
        //The OWL sender unit sends data every 6 seconds, so we poll every three 
        //so we don't miss an update.
        Thread.sleep(3000);
      }
    }
    catch(Exception e)
    {
      electricOWLController.error(e);
    }
    finally
    {
      if( br != null )
      {
        try
        {
          br.close();
        }
        catch(Exception e) {}
        br=null;
      }
      if( bw != null )
      {
        try
        {
          bw.close();
        }
        catch(Exception e) {}
        bw=null;
      }
      if( socket != null )
      {
        try
        {
          socket.close();
        }
        catch(Exception e) {}
        socket=null;
      }
    }
  }
  
  public synchronized void clearLog()
  {
    long startTime = System.currentTimeMillis();
    String line;
    
    try
    {
      bw.write("ERASELOG\n");
      bw.flush();
      while(true)
      {
        line=br.readLine();
        if( line == null )
        {
          throw new Exception("OWL server connected connection closed unexpectedly when clearing the log.");
        }
        //If erase log file complete
        if( line.indexOf("ERASELOG_COMPLETE") != -1 )
        {
          break;
        }
        //If error
        if( line.indexOf("ERROR") != -1 )
        {
          throw new Exception(line);
        }
        if( System.currentTimeMillis() > startTime+10000 )
        {
          throw new Exception("Timeout when attempting to clear the log.");
        }
      }
    }
    catch(Exception e)
    {
      electricOWLController.error(e);
    }
  }
  
  /**
   * Init the USB connect device attached to the server
   */
  public void initUSBConnect() throws Exception
  {
    long startTime = System.currentTimeMillis();
    String line;
    
    electricOWLController.info("Attempting to initialise the USB connect device attached to the server");
    bw.write("INIT\n");
    bw.flush();
    while(true)
    {
      line=br.readLine();
      if( line == null )
      {
        throw new Exception("OWL server connected connection closed unexpectedly when initialising the USB connect device.");
      }
      //If init completed successfully
      if( line.indexOf("INIT_SUCCESS") != -1 )
      {
        electricOWLController.info("USB connect device successfully initialised.");
        break;
      }
      if( line.indexOf("USB_CONNECT_UNAVAILABLE") != -1 )
      {
        electricOWLController.info("The USB connect device is currently unavailable.");
        break;
      }
      //If error
      if( line.indexOf("ERROR") != -1 )
      {
        throw new Exception(line);
      }
      if( System.currentTimeMillis() > startTime+10000 )
      {
        throw new Exception("Timeout when attempting to initialise the USB connect device.");
      }
    }
  }
  
  /**
   * Get the size of the log file
   * 
   * @return The size of the log file in bytes.
   */
  public synchronized int getLogSize()
  {
    int logFileSize=0;
    
    String line;
    try
    {
      bw.write("LOGSIZE_GET\n");
      bw.flush();
      line=br.readLine();
      String elems[] = line.split("=");
      if( elems.length > 1 )
      {
        try
        {
          logFileSize=Integer.parseInt(elems[1]);
        }
        catch(NumberFormatException e) {}
      }
    }
    catch(Exception e)
    {
      electricOWLController.error(e);
    }
    
    return logFileSize;
  }

  /**
   * Get the data (over the required period of time) and plot it on the logFrame
   * 
   * @param startDate The date/time of the first data to plot
   * @param stopDate  The date/time of the last data to plot.
   * 
   * If both startDate and stopDate are null then all the log data is plotted.
   */
  public synchronized void plot(Date startDate, Date stopDate, long plotPeriodMS, int sensorAddress, PlotFrame plotFrame)
  {
	Runtime runTime = Runtime.getRuntime();
	long initialFreeMemory;
	double memoryReductionFactor;
	String sensorNameForPlot="unknown";
	
	Hashtable<String, Integer> sensorTable = ElectricOWLController.GetSensorHashTable();
    for( String sensorName : sensorTable.keySet() )
    {
      if( sensorTable.get(sensorName) == sensorAddress )
      {
    	  sensorNameForPlot = sensorName; 
      }
    }

    ProgressMonitor progressMonitor = new ProgressMonitor(plotFrame, "", "",0,0);
    int readingCount=0;
    String line;
    boolean foundData=false;
    String plotPeriodString;
    
    if( plotPeriodMS == 1000 )
    {
      plotPeriodString="SECONDS";
    }
    else if( plotPeriodMS == 1000*60 )
    {
      plotPeriodString="MINUTES";
    }
    else if( plotPeriodMS == 1000*60*60 )
    {
      plotPeriodString="HOURS";
    }
    else if( plotPeriodMS == 1000*60*60*24 )
    {
      plotPeriodString="DAYS";
    }
    else if( plotPeriodMS == 1000*60*60*24*7 )
    {
      plotPeriodString="WEEKS";
    }
    else{
      Dialogs.showErrorDialog(plotFrame, "Error", plotPeriodMS+" is an invalid plotPeriodMS");
      return;
    }
    //Set max percentage as 100 % for the progress
    progressMonitor.setMaximum(100);
    progressMonitor.setMillisToDecideToPopup(0);
    progressMonitor.setProgress(0);

    initialFreeMemory = runTime.freeMemory();
    
    long t1 = System.currentTimeMillis();
    try
    {
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
      bw.write("LOG_GET;"+simpleDateFormat.format( startDate )+";"+simpleDateFormat.format( stopDate )+";"+plotPeriodString+";"+sensorAddress+"\n");
      bw.flush();
      String m="Searching the data on the OWLServer, please wait...";
      electricOWLController.info(m);
      progressMonitor.setNote(m);
      while(true)
      {
   		line=br.readLine();
        if( line == null )
        {
          throw new Exception("OWL server connected connection closed unexpectedly when retrieving the log.");
        }
        if( progressMonitor.isCanceled() )
        {
        	cancelGetLog();
        	break;        	
        }
        //If this message provides an indication as to how far the server is through the task of searching the log file.
        if( line.indexOf("PERCENTAGE") != -1 )
        {
        	StringTokenizer strTok = new StringTokenizer(line, "=");
        	if( strTok.countTokens() == 2 )
        	{
        		strTok.nextToken();
        		String percentageString = strTok.nextToken();
        		progressMonitor.setProgress( Integer.parseInt( percentageString ) );
        	}
        	continue;
        }
   		
        //Calc the factor by which the free memory in the JVM has reduced.
        memoryReductionFactor = (double)runTime.freeMemory()/(double)initialFreeMemory;
        
        //If 
        //- we have to many plot points to be displayed or
        //- The free memory has droped below 1 MB or
        //- The free memory has reduced to 10% of that we had before attempting the plot
        if( readingCount >= MAX_HISTORY_PLOT_POINT_COUNT || runTime.freeMemory() < 1024000 || memoryReductionFactor < 0.1 )
        {
          if( readingCount >= MAX_HISTORY_PLOT_POINT_COUNT ) 
          {
        	  Dialogs.showErrorDialog(plotFrame, "Warning", "The plot has reached the maximum size, not all the data you asked for will be plotted.\nPlease reduce the resolution to plot the entire time span.");
          }
          //Cancel the progress dialog
          progressMonitor.setMaximum(100);
          cancelGetLog();
          break;
        }
        
        //If erase log file complete
        if( line.indexOf("LOG_GET_COMPLETE") != -1 )
        {
          break;
        }
        //If error
        if( line.indexOf("ERROR") != -1 )
        {
          throw new Exception(line);
        }
        //If not cancelled, add to plot
        if( !progressMonitor.isCanceled() )
        {
          if( !foundData )
          {
            foundData=true;
            m="Reading data from OWLServer";
            electricOWLController.info(m);
            progressMonitor.setNote(m);
          }
          SensorData sensorData = new SensorData(line);
          plotFrame.plotData(sensorData, sensorNameForPlot);   
          readingCount++;
        }
      }
      if( !progressMonitor.isCanceled() && foundData )
      {
        long t2=System.currentTimeMillis();
        electricOWLController.info("It took "+(((t2-t1)/1000))+" seconds to read and plot the data from the OWLServer");
        
        //Now update the plot points on the GUI
        plotFrame.setNotify(true);
        long t3=System.currentTimeMillis();
        electricOWLController.info("It took "+(((t3-t2)/1000))+" seconds to process the data from the OWLServer"); 
        //Make visible once all data has been added to the plot, much quicker
        plotFrame.setVisible(true);
      }
      if( !foundData && !progressMonitor.isCanceled() )
      {
        Dialogs.showOKDialog(plotFrame, "Open History", "No data found for dates selected for sensor "+sensorAddress);
      }
    }
    catch(Exception e)
    {
      electricOWLController.error(e);
    }
    finally
    {
        //Cancel the progress dialog
        progressMonitor.setProgress(100);   
    }
  }
  
  /**
   * Called to get the server to stop sending log data once LOG_GET command has been called.
   * 
   */
  private void cancelGetLog() throws IOException
  {  
	  long repeatCancelMessagePeriodMS = 500;
	  //Send cancel message to server
      bw.write("CANCEL");
      bw.flush();
	  //Wait until we stop receiving data from the server for at least 1 second
      //to drain any data already sent by the server but not received by the client
      long timeout=System.currentTimeMillis()+1000;
      long sendCancelTimeout=System.currentTimeMillis()+repeatCancelMessagePeriodMS;
      while( true )
      {
    	  if( br.ready() ) 
    	  {
    		  br.readLine();
    		  //d("cancelGetLog(): "+line);
    		  timeout=System.currentTimeMillis()+1000;
    	  }
    	  if( System.currentTimeMillis() > timeout )
    	  {
    		  break;
    	  }
    	  //Periodically send another cancel message to shout at the server to shut up
    	  if( System.currentTimeMillis() > sendCancelTimeout ) 
    	  {
    		  //Send cancel message to server
    	      bw.write("CANCEL");   
    	      bw.flush();
    	      sendCancelTimeout=System.currentTimeMillis()+repeatCancelMessagePeriodMS;
    	  }
      }
  }
}
