/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package eowl.model;

import java.util.StringTokenizer;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/**
 * Holds a single sample of data from an OWL sensor
 */
public class SensorData
{
  private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  private Date           date;
  private String 		 dateString;
  private int            address;
  private double         amps;
  private String         model;
  private double         accumulatedAmps;
  
  /**
   * Create a sensor data object given the message received by the server.
   * 
   * @param serverMessage
   */
  public SensorData(String serverMessage) throws SensorDataFormatException
  {
    boolean formatOK=false;
    int pos=serverMessage.indexOf('=');
    if( pos > 0 )
    {
      StringTokenizer strTok = new StringTokenizer(serverMessage.substring(pos+1), ";");
      if( strTok.countTokens() == 5 )
      {
        try
        {
          address = Integer.parseInt( strTok.nextToken() );
          amps = getDouble( strTok.nextToken() );
          model = strTok.nextToken();
          accumulatedAmps = getDouble( strTok.nextToken() );
          //Get the date at which this object was created
          dateString = strTok.nextToken();     
          try
          {
            date = df.parse( dateString );
          }
          catch(ParseException e)
          {
            throw new SensorDataFormatException("Date format text format error: "+dateString);
          }
          formatOK=true;
        }
        catch(NumberFormatException e) {}
      }
    }
    if( !formatOK )
    {
      throw new SensorDataFormatException(serverMessage+" is not formatted correctly");
    }
  }
  
  private double getDouble(String valueString)
  {
      //If we have comma separated numbers (some localisations do this) convert 
      //them to . separated numbers.
	  if( valueString.contains(",") )
	  {
		  valueString = valueString.replace(',', '.');
	  }
	  return Double.parseDouble( valueString );
  }
  
  /**
   * Constructor
   * 
   * Used to construct a dummy sensor data object
   * @throws SensorDataFormatException
   */
  public SensorData() throws SensorDataFormatException {
	  //Create a dummy SensorData object
	  this("SENSOR_DATA=383,2.59,19,79248740.27,2010-06-19 07:01:41");
  }
  
  public String toString()
  {
    StringBuffer strBuf = new StringBuffer();
    strBuf.append("DATE="+date);
    strBuf.append(",ADDR="+address);
    strBuf.append(",AMPS="+amps);
    strBuf.append(",MODEL="+model);
    strBuf.append(",ACCUMULATED_AMPS="+accumulatedAmps);
    return strBuf.toString();
  }
  
  /**
   * Return the address of the sensor that provided this data
   * @return
   */
  public int getAddress()
  {
    return address;
  }
  
  /**
   * Return the power in Amps
   * @return
   */
  public double getAmps()
  {
    return amps;
  }
  
  /**
   * Get the OWL model string.
   * 
   * @return
   */
  public String getModel()
  {
    return model;
  }
  
  /**
   * Get the accumulated amps field ?
   * 
   * @return
   */
  public double getAccumulatedAmps()
  {
    return accumulatedAmps;
  }
  
  /**
   * Get the date that the sensor data was read
   * 
   * @return
   */
  public Date getDate()
  {
    return date;
  }
  
  /**
   * Return the data as a string in the format yyyy-MM-dd HH:mm:ss
   * 
   * @return The date string
   */
  public String getDateString() {
	  return dateString;
  }
  
  /**
   * Return the power in KW
   * @return
   */
  public double getKW(double voltage)
  {
    return (amps*voltage)/1E3;
  }
  
  public double getCost(double voltage, double costPerkWh)
  {
    double kw = getKW(voltage);
    return costPerkWh*kw;
  }
}
