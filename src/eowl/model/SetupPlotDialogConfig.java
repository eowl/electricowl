/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package eowl.model;

import lib.io.SimpleConfig;

public class SetupPlotDialogConfig extends SimpleConfig
{
  public int xPos=-1;
  public int yPos=-1;
  
  public int startMin=0;
  public int startHour=0;
  public int startDay=0;
  public int startMonth=0;
  public int startYear=2000;
 
  public int stopMin=0;
  public int stopHour=0;
  public int stopDay=0;
  public int stopMonth=0;
  public int stopYear=2000;
  
  public int plotPeriodMS=1000*60;
  public int selectedSensorIndex=0;
}
