/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package eowl.model;

import java.io.*;

/**
 * Thrown when a message is received from the OWL server with incorrect formatting.
 */
public class SensorDataFormatException extends IOException
{
  public SensorDataFormatException(String message)
  {
    super(message);
  }
}
