/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package eowl.model;

import lib.io.SimpleConfig;

public class GUIConfig extends SimpleConfig
{
  public int fWidth=1024;
  public int fHeight=768;
  public int xPos=-1;
  public int yPos=-1;
  
}
