/**********************************************************************************
 *                        Copyright 2009 Paul Austen                              *
 *                                                                                *
 * This program is distributed under the terms of the GNU General Public License  *
 **********************************************************************************/
package eowl.model;

import lib.io.SimpleConfig;
import java.util.*;
import java.io.*;

public class ControllerConfig extends SimpleConfig
{
  public boolean                    runLocalOWLServer   	=   false;
  public String                     serverAddress       	=   "127.0.0.1";
  public int                        serverPort          	=   12745;
  public String                     sensorAddrNames     	=   "";
  public double                     voltage             	=   230;
  public boolean                    viewAmps            	=   false;
  public boolean                    viewkWh             	=   true;
  public boolean                    viewCost            	=   false;
  public double                     costPerkWh          	=   0.01;
  public long                       maxPlotAgeSecs      	=   600;
  public String                     currencyString      	=   "�";      
  public int                        lookAndFeelIndex    	=   0;
  public File                       logFile             	=   new File(System.getProperty("user.home")+System.getProperty("file.separator")+"electric_owl.log");
  public long                       logFileDays         	=   7;
  public boolean					maxPowerTriggerEnabled 	= 	false;
  public double						maxPowerTriggerKWh      = 	15.0;
  public String						maxPowerExtCmd          =   "";
  
  /**
   * Get the sensor names for the given address as entered by the user previously.
   * 
   * @param sensorAddress
   * 
   * @return
   */
  public String getSensorName(int sensorAddress)
  {
    StringTokenizer strTok;
    String          addressString;
    String          name;
       
    Scanner keyValuePair = new Scanner(sensorAddrNames).useDelimiter(",");
    while( keyValuePair.hasNext() )
    {
      strTok = new StringTokenizer(keyValuePair.next(),"=");
      if( strTok.countTokens() == 2 )
      {
        addressString=strTok.nextToken();
        name=strTok.nextToken();
        if( addressString.equals(""+sensorAddress) )
        {
          return name;
        }
      }
    }
    return null;
  }

  /**
   * Get an array containing all the sensor names.
   * 
   * @return The String array
   */
  public String[] getSensorNames()
  {
    Vector<String>nameList = new Vector<String>();
    
    StringTokenizer strTok;
    String          name;
       
    Scanner keyValuePair = new Scanner(sensorAddrNames).useDelimiter(",");
    while( keyValuePair.hasNext() )
    {
      strTok = new StringTokenizer(keyValuePair.next(),"=");
      if( strTok.countTokens() == 2 )
      {
        strTok.nextToken();
        name=strTok.nextToken();
        nameList.add(name);
      }
    }
    String nameArray[] = new String[nameList.size()];
    for( int i=0 ; i< nameList.size() ; i++ )
    {
      nameArray[i]=nameList.get(i);
    }
    return nameArray;
  }
  
  /**
   * Get a Hashtable containing the sensor details.
   * 
   * The key to the hash table is the name of the sensor (given by the user)
   * The value is the address of the sensor
   * 
   * @return The Hashtable
   */
  public Hashtable<String, Integer> getSensorTables()
  {
    Hashtable<String, Integer>sensorTable = new Hashtable<String, Integer>();
    
    StringTokenizer strTok;
    int             address;
    String          name;
       
    Scanner keyValuePair = new Scanner(sensorAddrNames).useDelimiter(",");
    while( keyValuePair.hasNext() )
    {
      strTok = new StringTokenizer(keyValuePair.next(),"=");
      if( strTok.countTokens() == 2 )
      {
        try
        {
          address=Integer.parseInt( strTok.nextToken() );
          name=strTok.nextToken();
          sensorTable.put(name, address);
        }
        catch(NumberFormatException e)
        {
          e.printStackTrace();
        }  
      }
    }
    return sensorTable;
  }

}
