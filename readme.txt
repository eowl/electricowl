﻿Building the ElectricOWL installer software.
The following programs must be installed on you windows platform in 
order to build the ElectricOWL installer.

- SharpDevelop. Available from http://www.icsharpcode.net/OpenSource/SD/Download/
- Eclipse. Available from http://www.eclipse.org/downloads/
- Inno Setup. Avaialble from http://www.jrsoftware.org/isdl.php#stable

The steps required to build the ElectricOWL installer are detailed below.

- The OWL server should be built using SharpDevelop. 
  To do this open the OWLServer/OwlServer.sln project file from within 
  SharpDevelop and Select Build (menu bar option), Rebuild solution. 
  This creates the executable file that is pulled into the installer.
 
- The Electric OWL software can be built using the eclipse IDE. 
  To do this open the location root location for the Electric OWL 
  files as the workspace (you are prompted to select a workspace 
  path when eclipse is started).  Once you have the project opened 
  in eclipse select Project (menu bar option), clean option. The 
  project should then be rebuilt, right click on the electricOWL.jardesc
  file from the package explorer window (left hand side of IDE). 
  Select create jar from the pull-down menu list. Select the Yes 
  button on the dialog that is presented to overwrite the existing jar 
  file. This creates the jar file that is pulled into the installer.

- The windows installer can be built using Inno Setup. You should
  have completed both the above steps before an installer can be 
  built. To build the installer, double click on the electricowl.iss 
  file, this will open the installer configuration file inside Inno 
  Setup. Select the compile icon from the tool-bar. The installer
  file is then created in the output directory.
