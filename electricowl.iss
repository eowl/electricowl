
[Setup]
OutputBaseFilename=Electric OWL V1.8

AppName=Electric OWL
AppVerName=Electric OWL V1.8
DefaultDirName={pf}\EOWL
DisableDirPage=yes
DefaultGroupName= Electric OWL

[Components]
Name: "main"; Description: "Main Files"; Types: full compact custom; Flags: fixed

[Tasks]
Name: desktopicon; Description: "Create a &desktop icon"; GroupDescription: "Additional icons:"; Components: main
Name: desktopicon\common; Description: "For all users"; GroupDescription: "Additional icons:"; Components: main; Flags: exclusive
Name: desktopicon\user; Description: "For the current user only"; GroupDescription: "Additional icons:"; Components: main; Flags: exclusive unchecked
name: "startupicon"; Description: "Run OWLServer on Startup"; GroupDescription: "Additional tasks:"; MinVersion: 4,4; Flags: unchecked
;Name: quicklaunchicon; Description: "Create a &Quick Launch icon"; GroupDescription: "Additional icons:"; Components: main; Flags: unchecked
;Name: associate; Description: "&Associate files"; GroupDescription: "Other tasks:"; Flags: unchecked

[Files]
Source: "electricowl.jar"; DestDir: "{app}"; Flags: ignoreversion
Source: "jfreechart.jar"; DestDir: "{app}"; Flags: ignoreversion
Source: "api.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "../OWLServer/bin/Release/OwlServer.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "tenxhid.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "electricowl.bat"; DestDir: "{app}"; Flags: ignoreversion
Source: "world.ico"; DestDir: "{app}"; Flags: ignoreversion
Source: "electric_owl_manual.pdf"; DestDir: "{app}"; Flags: ignoreversion

[Icons]
Name: "{group}\Electric OWL"; Filename: "{app}\electricowl.bat"; Flags: runminimized ; WorkingDir: "{app}" ; IconFilename: "{app}\world.ico"
Name: "{group}\Electric OWL Manual"; Filename: "{app}\electric_owl_manual.pdf"; WorkingDir: "{app}" ; IconFilename: "{app}\world.ico"
Name: "{group}\OWL server"; Filename: "{app}\OwlServer.exe"; WorkingDir: "{app}" ; IconFilename: "{app}\world.ico"
Name: "{userdesktop}\OWL server"; Filename: "{app}\OwlServer.exe"; Tasks: desktopicon ; IconFilename: "{app}\world.ico"
Name: "{userstartup}\OWLServer"; Filename: "{app}\OwlServer.exe";Tasks: startupicon; workingdir: "{app}"
Name: "{group}\Uninstall"; Filename: "{uninstallexe}"

[Run]
Filename: "{app}\OwlServer.exe"; Description: "Launch OWL Server"; Flags: nowait postinstall skipifsilent

[Code]
//This code checks for a particular version of Java to be installed before the program is installed
//Change minJREVersionRegistry to the require version number
function GetJREVersion(): String;
begin
  RegQueryStringValue( HKLM, 'SOFTWARE\JavaSoft\Java Runtime Environment', 'CurrentVersion', Result );
end; // END_FUNC GetJREVersion
function InitializeSetup( ): Boolean;
var
  jreVersion:             String;
  minJREVersionRegistry:  String;
begin
  Result := true;
  //
  // Initialize min java version constant
  //
  minJREVersionRegistry := '1.6'
  //
  //
  // now we check the version of the installed JRE:
  jreVersion := GetJREVersion();
  if ( jreVersion = '' ) then
    begin
    MsgBox( 'Java is not installed on this computer and this program needs it.' +
         'Please install Java of at least least version ' + minJREVersionRegistry + ' and then re-install this program.',
         mbError, MB_OK );
    Result := false;
    end
  else
    if ( jreVersion < minJREVersionRegistry ) then
      begin
      MsgBox( 'The Java version is too low. ' +
         'This program requires a minimum Java version of ' +  minJREVersionRegistry + '. The installed Java version is ' + jreVersion + '. Please install Java of at least version ' + minJREVersionRegistry + ' and then re-install this program.',
         mbError, MB_OK );
      Result := false;
      end
    else
      Result := true;
end; // END_FUNC InitializeSetup




